use std::cell::RefCell;
use std::fmt::Write;
use std::ops::{Deref, DerefMut};
use std::rc::Rc;
use std::sync::{Arc, Mutex, MutexGuard};
use fltk::button::{Button, CheckButton};
use fltk::draw::{begin_line, draw_box, draw_line, draw_rbox, draw_rect, draw_rect_fill, draw_rectf, draw_rounded_rect, draw_rounded_rectf, draw_text2, end_line, set_draw_color};
use fltk::enums::{Align, CallbackTrigger, Color, Event, Font, FrameType, LabelType};
use fltk::frame::Frame;
use fltk::group::{Flex, FlexType, Group, HGrid, Pack, PackType, VGrid};
use fltk::input::Input;
use fltk::prelude::{ButtonExt, GroupExt, ImageExt, InputExt, TableExt, WidgetBase, WidgetExt, WindowExt};
use fltk::{app, draw, enums, group, table, window};
use fltk::app::{event, MouseButton};
use fltk::enums::FrameType::FlatBox;
use fltk::image::{PngImage, SvgImage};
use fltk::misc::InputChoice;
use fltk::table::Table;
use crate::svg_common::SVG;

pub fn center(w: i32, h: i32) -> (i32, i32) {
    (
        (app::screen_size().0 / 2.0) as i32 - w / 2,
        (app::screen_size().1 / 2.0) as i32 - h / 2,
    )
}

//灰色
pub(crate) const global_border_color: Color = Color::from_rgb(215, 217, 224);
pub(crate) const BTN_CONFIRM:Color = Color::from_rgb(179, 36, 36);

#[derive(Debug, Clone)]
pub struct MGroupInput {
    t_widget: fltk::group::Group,
    t_input_widget: fltk::input::Input,
    t_title_widget: fltk::frame::Frame,
    t_param: Vec<(i32, i32)>,
    original_value: Arc<Mutex<String>>,
}


impl MGroupInput {
    pub fn new(x: i32, y: i32, w: i32, h: i32) -> Self {
        let mut group = Group::new(x, y, w, h, "");
        group.set_frame(FrameType::FlatBox);
        group.set_color(Color::from_rgb(236, 236, 236));
        // group.draw(|b|{
        //     // draw_rect_fill(b.x(),b.y(),b.w()+4,b.h(),Color::White);
        //     set_draw_color(b.color());
        //     let (x1,x2,y1,y2) = (b.x(),b.x()+b.w(),b.y(),b.y()+b.h()+2);
        //     draw_line(x1,y1,x2,y1);
        //     draw_line(x2,y1,x2,y2);
        //     draw_line(x2,y2,x1,y2);
        //     draw_line(x1,y2,x1,y1);
        // });

        let mut title = Frame::new(x + 1, y, 0, h - 2, "").center_y(&group);
        title.set_frame(FrameType::FlatBox);
        title.set_color(Color::White);


        let mut input = Input::default().with_size(w - 1, h - 2).with_pos(x, y).center_y(&group);
        input.set_frame(FrameType::FlatBox);
        input.set_color(Color::White);
        group.end();

        let tuple = vec![(title.w(), title.h()), (input.w(), input.h())];
        Self {
            t_widget: group,
            t_input_widget: input,
            t_title_widget: title,
            t_param: tuple,
            original_value: Arc::new(Mutex::new(String::new())),
        }
    }

    pub fn value(&self) -> String {
        self.t_input_widget.value()
    }
    pub fn set_title(&mut self, w: i32, title: &str) {

        self.t_title_widget.set_size(w, self.t_title_widget.h());
        self.t_title_widget.set_label(title);
        self.t_param[0] = (self.t_title_widget.w(), self.t_title_widget.h());

        self.flush();
    }

    pub fn bind_value_arc_mutex(&mut self, value_arc_mutex: Arc<Mutex<String>>) {
        //如果有值，先绑定值
        let _ = {
            let mut v = value_arc_mutex.lock().unwrap();
            v.clear();
            v.push_str(self.t_input_widget.value().as_str());
        };
        //值发生变更，同步更新到绑定的实例上
        self.t_input_widget.set_callback(move |input| {
            let mut v = value_arc_mutex.lock().unwrap();
            v.clear();
            v.push_str(input.value().as_str());
        });
    }
    pub fn set_value(&mut self, v: &str) {
        {
            let mut bind = self.original_value.lock().unwrap();
            bind.clear();
            bind.push_str(v.clone());
        }
        self.t_input_widget.set_value(v);
        self.t_input_widget.do_callback();
    }

    pub fn original_value(&self) -> Arc<Mutex<String>> {
        self.original_value.clone()
    }

    pub fn x(&self) -> i32 {
        self.t_widget.x()
    }
    pub fn y(&self) -> i32 {
        self.t_widget.y()
    }
    pub fn w(&self) -> i32 {
        self.t_widget.w()
    }
    pub fn h(&self) -> i32 {
        self.t_widget.h()
    }
    pub fn set_color(&mut self, color: Color) {
        self.t_widget.set_color(color);
    }
    pub fn set_title_color(&mut self, color: Color) {
        self.t_title_widget.set_frame(FrameType::FlatBox);
        self.t_title_widget.set_color(color);
    }
    pub fn add<W: WidgetExt>(&mut self, w: &W) {
        self.t_widget.add(w);
        self.t_param.push((w.w(), w.h()));
        self.flush();
    }
    pub fn flush(&mut self) {

        self.t_title_widget.set_size(self.t_param[0].0, self.t_param[0].1);

        //输入框自动缩放，其他控件固定
        let mut other_len = 0;
        for i in 0..self.t_param.len() {
            if i == 1 {
                continue;
            }
            let (w, h) = self.t_param[i];
            other_len += w + 4;//间隔2
        }

        let x = self.t_title_widget.x() + self.t_title_widget.w();

        self.t_input_widget.set_pos(x, self.t_input_widget.y());
        self.t_input_widget.set_size(self.w() - other_len - 1, self.t_input_widget.h());


        if self.t_widget.children() >= 2 {
            let mut next_pos = self.t_input_widget.x() + self.t_input_widget.w() + 2;
            for i in 2..self.t_widget.children() {
                if let Some(mut widget) = self.t_widget.child(i) {
                    let (w, h) = self.t_param[i as usize];
                    widget.set_pos(next_pos, self.y() + ((self.h() - widget.h()) / 2));
                    widget.set_size(w, h);
                    next_pos = next_pos + widget.w() + 2;
                }
            }
        }
        self.t_widget.redraw();
    }
}

impl Deref for MGroupInput {
    type Target = fltk::group::Group;
    fn deref(&self) -> &Self::Target {
        &self.t_widget
    }
}

impl DerefMut for MGroupInput {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.t_widget
    }
}

pub trait M_Table{
    fn draw_header(&self,txt: &str,r:i32,c:i32, x: i32, y: i32, w: i32, h: i32);
    fn draw_cell_content(&self, txt: &str,r:i32,c:i32, x: i32, y: i32, w: i32, h: i32, selected: bool);
    fn draw_cell_svg(&self,svg: &mut SvgImage, x: i32, y: i32, w: i32, h: i32);
}

impl M_Table for Table {
    fn draw_header(&self, txt: &str,r:i32,c:i32, x: i32, y: i32, w: i32, h: i32) {
        draw::draw_rectf(x, y, w, h);
        draw::push_clip(x, y, w, h);

        if r>-1 && r%2==0 {
            draw::draw_box(FlatBox,x,y,w,h,Color::from_rgb(243, 246, 248));
        }else{
            draw::draw_box(FlatBox,x,y,w,h,Color::White);
        }

        draw::set_draw_color(enums::Color::Black);
        draw::set_font(enums::Font::Helvetica, 14);
        draw::draw_text2(txt, x, y, w, h, enums::Align::Center);
        set_draw_color(table_border_color);
        draw::draw_line(x, y, x + w, y);
        draw::draw_line(x, y, x, y + h);
        draw::pop_clip();

    }
    fn draw_cell_svg(&self,svg: &mut SvgImage, x: i32, y: i32, w: i32, h: i32) {
        svg.draw(x, y, w, h);
    }
    fn draw_cell_content(&self, txt: &str,r:i32,c:i32, x: i32, y: i32, w: i32, h: i32, selected: bool) {
        draw::push_clip(x, y, w, h);

        if selected {

            if  r>-1 && r%2==0 {
                draw::set_draw_color(Color::from_rgb(243, 246, 248));
            }else{
                draw::set_draw_color(Color::White);
            }

        } else {
            if  r>-1 && r%2==0 {
                draw::draw_box(FlatBox,x,y,w,h,Color::from_rgb(243, 246, 248));
            }else{
                draw::draw_box(FlatBox,x,y,w,h,Color::White);
            }
        }

        draw::draw_rectf(x, y, w, h);
        draw::set_draw_color(enums::Color::Gray0);
        draw::set_font(enums::Font::Helvetica, 14);
        draw::draw_text2(txt, x + 5, y, w, h, enums::Align::Left);

        set_draw_color(table_border_color);
        draw::draw_line(x, y, x + w, y);
        draw::draw_line(x, y, x, y + h);
        draw::pop_clip();
    }
}

pub struct MTable {
    t_widget: table::Table,
    headers: Vec<&'static str>,
    data: Arc<Mutex<Vec<Vec<String>>>>,
    edit_input: Input,
    not_edit_col: Rc<RefCell<Vec<i32>>>,
}

const table_border_color: Color = Color::from_rgb(220, 220, 220);

impl MTable {
    pub fn new(x: i32, y: i32, w: i32, h: i32) -> Self {
        let mut table = table::Table::new(x, y, w, h, "");
        table.set_frame(FrameType::NoBox);
        table.set_color(Color::White);
        table.set_rows(0);
        table.set_row_header(true);
        table.set_row_resize(false);
        table.set_cols(0);
        table.set_col_header(true);
        table.set_col_header_height(28);
        table.set_col_width_all(200);
        table.set_col_resize(true);
        table.set_col_header_color(Color::from_rgb(236, 236, 236));

        let mut edit_input = Input::new(0,0,0,0,"");
        edit_input.set_frame(FrameType::FlatBox);
        edit_input.set_selection_color(Color::rgb_color(78, 110, 242));
        table.end();

        table.redraw();

        Self {
            t_widget: table,
            headers: vec![],
            data: Arc::new(Mutex::new(vec![])),
            edit_input,
            not_edit_col: Rc::new(RefCell::new(vec![])),
        }
    }

    pub fn edit_col(&mut self,col:i32){
        self.not_edit_col.borrow_mut().push(col);
    }
    pub fn set_headers(&mut self, headers: Vec<&'static str>) {
        self.headers = headers;
    }
    pub fn draw_cell<F>(&mut self, f: F) where F: Fn(&table::Table, &table::TableContext, &Vec<Vec<String>>, i32, i32, i32, i32, i32, i32) + 'static {
        let headers = self.headers.clone();
        let data: Arc<Mutex<Vec<Vec<String>>>> = self.data.clone();
        self.t_widget.draw_cell(move |t, ctx, row, col, x, y, w, h|
            match ctx {
                table::TableContext::StartPage => draw::set_font(enums::Font::Helvetica, 14),
                table::TableContext::ColHeader => {
                    t.draw_header(headers[col as usize],-1,-1, x, y, w, h);
                } // Column titles
                table::TableContext::RowHeader => t.draw_header(&format!("{}", row + 1),row,col, x, y, w, h), // Row titles
                table::TableContext::Cell => {
                    let data_op = data.lock().unwrap();
                    f(&t, &ctx, data_op.as_ref(), row, col, x, y, w, h);
                } // Data in cells
                _ => (),
            }
        );
        self.t_widget.draw(|t|{
            let row_nums = t.rows();
            let table_height = row_nums * t.row_height(0) + t.col_header_height();

            let col_nums = t.cols();
            let mut table_width = t.row_header_width();
            for i in 0..col_nums {
                table_width+=t.col_width(i)
            };
            set_draw_color(table_border_color);
            begin_line();
            draw_line(t.x(),t.y(),t.x()+table_width,t.y());
            draw_line(t.x()+table_width,t.y(),t.x()+table_width,t.y()+table_height);
            draw_line(t.x()+table_width,t.y()+table_height,t.x(),t.y()+table_height);
            draw_line(t.x(),t.y()+table_height,t.x(),t.y());
            end_line();
        });

    }

    pub fn draw_cell_svg(svg: &mut SvgImage, x: i32, y: i32, w: i32, h: i32) {
        svg.draw(x, y, w, h);
    }


    pub fn set_data(&mut self, data: Vec<Vec<String>>) {
        let mut bind = self.data.lock().unwrap();
        data.clone_into(bind.as_mut());
    }
    pub fn clear_data(&mut self) {
        let mut bind = self.data.lock().unwrap();
        bind.clear();
    }

    pub fn callback<F: FnMut(&mut table::Table, Arc<Mutex<Vec<Vec<String>>>>) + 'static>(&mut self, mut cb: F) {

        let data = self.data.clone();
        let mut edit_input = self.edit_input.clone();
        let not_edit_col = self.not_edit_col.clone();
        self.set_callback(move |t| {

            //处理展示编辑框
            if let Some((r, c, r2, c2)) = t.try_get_selection() {
                let not_edit_col = not_edit_col.borrow();
                let mut is_edit = true;
                if not_edit_col.len() >0 {
                    for i in not_edit_col.iter() {
                        if i == &c2 {
                            is_edit = false;
                            break;
                        }
                    }
                }
                //可以展示编辑框
                if is_edit {
                    let (x,y,w,h) = t.find_cell(t.callback_context(),r2,c2).unwrap();
                    let v = {
                        let bind = data.lock().unwrap();
                        let bind = bind.as_slice();
                        &bind[r2 as usize][c2 as usize].to_owned()
                    };
                    edit_input.set_pos(x+1,y+1);
                    edit_input.set_size(w-1,h-1);
                    edit_input.set_value(v.as_str());
                    edit_input.redraw();

                }

            }
            t.redraw();
            cb(t, data.clone());

        });
    }
}

impl Deref for MTable {
    type Target = table::Table;
    fn deref(&self) -> &Self::Target {
        &self.t_widget
    }
}

impl DerefMut for MTable {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.t_widget
    }
}


pub struct MDialog {
    self_widget: window::Window,
    content_group_widget: Group,
    btn_group_widget: Pack,
    btn_spacing: i32,
    txt: String,
}

impl MDialog {
    pub fn new(x: i32, y: i32, w: i32, h: i32, title: &'static str) -> Self {
        let mut modal_window = window::Window::new(x, y, w, h + 40, title);
        modal_window.make_modal(true);
        modal_window.set_color(Color::White);
        modal_window.make_resizable(false);
        let content = include_bytes!("assets/title.png");
        let res = PngImage::from_data(content);
        match res {
            Ok(img) => {
                modal_window.set_icon(Some(img));
            }
            Err(m) => {
                println!("error={}", m);
            }
        }

        let mut content_group = Group::new(0, 0, w, h, "");
        content_group.set_frame(FrameType::FlatBox);
        content_group.set_color(Color::White);

        content_group.end();

        let mut btn_group = Pack::new(0, h + 5, w, 30, "");
        btn_group.set_spacing(5);
        btn_group.set_type(PackType::Horizontal);
        let _ = Frame::default().with_size(w - 120, h);//占位，将按钮挤到右边去
        btn_group.end();

        Self {
            self_widget: modal_window,
            content_group_widget: content_group,
            btn_group_widget: btn_group,
            btn_spacing: 5,
            txt: String::default(),
        }
    }
    pub fn begin(&mut self){
        self.content_group_widget.begin();
    }
    pub fn end(&mut self){
        self.content_group_widget.end();
    }
    pub fn add_btn(&mut self, btn: &mut Button) {
        let mut btn1 = btn.clone();
        self.btn_group_widget.add(&btn1);

        let mut len = self.btn_spacing;
        for i in 1..self.btn_group_widget.children() {
            if let Some(btn) = self.btn_group_widget.child(i) {
                len += btn.w() + self.btn_spacing;
            }
        }
        if let Some(mut first) = self.btn_group_widget.child(0) {
            first.set_size(self.w() - len, first.h());
        }
    }

    pub fn set_text(&mut self, txt: &str) {
        let mut c = Frame::new(10, 0, self.w(), self.content_group_widget.h(), "");
        c.set_label(txt);
        c.set_align(Align::Left | Align::Inside);
        self.content_group_widget.add(&c);
    }
    pub fn add_content<W: WidgetExt>(&mut self, w: &W) {
        self.content_group_widget.add(w);
    }

    pub fn set_btn_spacing(&mut self, spacing: i32) {
        self.btn_spacing = spacing;
    }

    pub fn prompt(txt: &str) -> MDialog {
        let (x, y) = center(300, 50);
        let mut dialog = MDialog::new(x, y, 300, 60, "提示");
        dialog.set_text(txt);
        let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
        dialog.add_btn(&mut cancel);
        cancel.set_callback(|b| b.window().unwrap().hide());
        dialog
    }

    pub fn confirm<F: FnMut(&mut Button) + 'static>(txt: &str, cb: F) -> MDialog {
        let (x, y) = center(300, 50);
        let mut dialog = MDialog::new(x, y, 300, 60, "确定");
        dialog.set_text(txt);

        let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
        dialog.add_btn(&mut cancel);
        cancel.set_callback(|b| b.parent().unwrap().parent().unwrap().hide());

        let mut confirm = Button::miui_default().with_size(50, 20).with_label("确定");
        confirm.btn_color(BTN_CONFIRM);
        dialog.add_btn(&mut confirm);
        confirm.set_callback(cb);
        dialog
    }
    pub fn slide_prompt(txt: &str, x: i32, y: i32, w: i32, h: i32) {
        let (mut x, mut y, w, h) = (x, y, w, h);
        x = x + w / 2 - 100;
        let mut tip = window::Window::default()
            .with_pos(x, y)
            .with_size(200, 40);
        tip.make_modal(true);
        tip.make_resizable(false);
        tip.set_border(false);
        tip.set_override();
        let mut f = Frame::new(0, 0, 200, 40, "");
        f.set_label(txt);
        f.set_color(Color::rgb_color(240, 249, 235));
        f.set_frame(FrameType::FlatBox);
        f.set_label_color(Color::rgb_color(80, 194, 58));
        f.set_label_font(Font::HelveticaBold);

        f.set_label_size(15);
        let (l_w,l_h) = f.measure_label();
        if l_w+60 > w {
            tip.set_size(l_w+60,tip.h());
        }
        let mut img = SVG::check_circle(Some(Color::rgb_color(103, 194, 58)));
        f.draw(move |d| {
            draw_line(0, 0, d.w(), 0);
            draw_line(0, 1, d.w(), 1);
            img.draw(10, 12, 16, 16);
        });
        f.size_of(&tip);
        tip.end();
        tip.show();
        app::add_timeout3(0.001, move |h| {
            let mut y = tip.y();
            let ey = tip.y() + 10;
            while y < ey {
                app::sleep(0.005);
                tip.resize(tip.x(), tip.y() + 5, tip.w(), tip.h());
                y += 3;
            }
            app::sleep(1.0);
            tip.hide();
        });
    }
    pub fn slide_prompt_red(txt: &str, x: i32, y: i32, w: i32, h: i32) {
        let (mut x, mut y, w, h) = (x, y, w, h);
        x = x + w / 2 - 100;
        let mut tip = window::Window::default()
            .with_pos(x, y)
            .with_size(200, 40);
        tip.make_modal(true);
        tip.make_resizable(false);
        tip.set_border(false);
        tip.set_override();
        let mut f = Frame::new(0, 0, 200, 40, "");

        f.set_label(txt);
        f.set_color(Color::rgb_color(240, 214, 214));
        f.set_frame(FrameType::FlatBox);
        f.set_label_color(Color::Red);
        f.set_label_font(Font::HelveticaBold);

        f.set_label_size(15);
        //根据文字长宽，自适应
        let (l_w,l_h) = f.measure_label();
        if l_w+60 > w {
            tip.set_size(l_w+60,tip.h());
        }
        if l_h > h {
            tip.set_size(tip.w(),l_h+10);
        }

        let mut img = SVG::check_circle(Some(Color::Red));
        f.draw(move |d| {
            draw_line(0, 0, d.w(), 0);
            draw_line(0, 1, d.w(), 1);
            img.draw(10, 12, 16, 16);
        });
        f.size_of(&tip);
        tip.end();
        tip.show();
        app::add_timeout3(0.001, move |h| {
            let mut y = tip.y();
            let ey = tip.y() + 10;
            while y < ey {
                app::sleep(0.005);
                tip.resize(tip.x(), tip.y() + 5, tip.w(), tip.h());
                y += 3;
            }
            app::sleep(1.5);
            tip.hide();
        });
    }
}

impl Deref for MDialog {
    type Target = window::Window;
    fn deref(&self) -> &Self::Target {
        &self.self_widget
    }
}

impl DerefMut for MDialog {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.self_widget
    }
}

pub struct Label {
    self_widget: Frame,
}

impl Label {
    pub fn new(x: i32, y: i32, w: i32, h: i32, title: &'static str) -> Self {
        let mut self_widget = Frame::new(x, y, w, h, title);
        self_widget.set_align(Align::Center | Align::Left | Align::Inside);
        Self {
            self_widget
        }
    }
    pub fn default(title: &'static str) -> Self {
        Self {
            self_widget: Frame::default().with_label(title).with_align(Align::Center | Align::Left | Align::Inside)
        }
    }
    pub fn with_size(&mut self, w: i32, h: i32) -> &mut Self {
        self.self_widget.set_size(w, h);
        self
    }
    pub fn with_pos(&mut self, x: i32, y: i32) -> &mut Self {
        self.self_widget.set_pos(x, y);
        self
    }
    pub fn widget(&self) -> &Frame {
        &self.self_widget
    }
}

impl Deref for Label {
    type Target = Frame;
    fn deref(&self) -> &Self::Target {
        &self.self_widget
    }
}

impl DerefMut for Label {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.self_widget
    }
}

pub trait MCheckButton {
    fn miui(x: i32, y: i32, w: i32, h: i32) -> CheckButton;
    fn miui_default() -> CheckButton;
}

impl MCheckButton for CheckButton {
    fn miui(x: i32, y: i32, w: i32, h: i32) -> CheckButton {
        let mut button = CheckButton::miui_default();
        button.set_pos(x, y);
        button.set_size(w, h);
        button
    }

    fn miui_default() -> CheckButton {
        let mut button = CheckButton::default();
        button.set_frame(FrameType::NoBox);
        button.visible_focus(false);
        let mut svg2 = SVG::check_mark(Some(Color::Black));
        let mut p_value = true;
        button.draw(move |b| {
            draw_rect_fill(b.x() - 1, b.y() - 1, b.w() + 5, b.h(), b.parent().unwrap().color());
            draw_rect_fill(b.x() + 2, b.y() + 2, b.w() - 2, b.h() - 2, Color::White);
            set_draw_color(b.color());
            let (x1, x2, y1, y2) = (b.x(), b.x() + b.w(), b.y(), b.y() + b.h());
            draw_line(x1, y1, x2, y1);
            draw_line(x2, y1, x2, y2);
            draw_line(x2, y2, x1, y2);
            draw_line(x1, y2, x1, y1);
            if b.value() {
                svg2.scale(b.w() - 4, b.h() - 4, false, false);
                svg2.draw(b.x() + 2, b.y() + 2, b.w(), b.h());
            }
        });
        button
    }
}

pub struct Checkbox {
    self_widget: Button,
    value: Rc<RefCell<bool>>,
}

impl Checkbox {
    pub fn new(x: i32, y: i32, w: i32, h: i32, title: &'static str) -> Self {
        let mut f = Button::new(x, y, w, h, title);
        let value = Rc::new(RefCell::new(false));
        f.set_frame(FrameType::FlatBox);
        f.set_color(Color::White);
        f.set_selection_color(Color::rgb_color(78, 110, 242));
        f.visible_focus(false);

        let value_cp = value.clone();
        f.set_callback(move |f| {
            let mut v = value_cp.borrow_mut();
            *v = !*v;
        });

        let mut svg2 = SVG::check_mark(Some(Color::White));
        let value_cp = value.clone();
        let mut p_value = true;
        f.draw(move |f| {
            let v = value_cp.borrow();
            if p_value == *v {

                return;
            }
            // draw_rect_fill(f.x()+1, f.y()+1, f.w()+1, f.h()+1,f.parent().unwrap().color());
            draw_rect_fill(f.x(), f.y(), f.w(), f.h(), Color::White);

            set_draw_color(f.color());
            let (x1, x2, y1, y2) = (f.x(), f.x() + f.w(), f.y(), f.y() + f.h());
            draw_line(x1, y1, x2, y1);
            draw_line(x2, y1, x2, y2);
            draw_line(x2, y2, x1, y2);
            draw_line(x1, y2, x1, y1);

            if *v {
                draw_rect_fill(f.x(), f.y(), f.w(), f.h(), Color::rgb_color(78, 110, 242));
                // svg2.scale(f.w()-4, f.h()-4, false, false);
                // svg2.draw(f.x()+2, f.y()+2, f.w(), f.h());
            }
            p_value = *v;

        });

        Self {
            self_widget: f,
            value,
        }
    }
    pub fn default() -> Self {
        Checkbox::new(0, 0, 0, 0, "")
    }
    pub fn value(&self) -> bool {
        self.value.borrow().to_owned()
    }
    pub fn with_size(&mut self, w: i32, h: i32) -> &mut Self {
        self.with_size(w, h);
        self
    }
}

impl Deref for Checkbox {
    type Target = Button;
    fn deref(&self) -> &Self::Target {
        &self.self_widget
    }
}

impl DerefMut for Checkbox {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.self_widget
    }
}


struct param {
    x: i32,
    y: i32,
    w: i32,
    h: i32,
    title: String,
    frame: Option<Frame>,
    label_color: Option<Color>,
}

pub trait MButton {
    fn miui(x: i32, y: i32, w: i32, h: i32, title: &'static str) -> Button;
    fn miui_default() -> Button;
    fn btn_color(&mut self, color: Color);
}

impl MButton for Button {
    fn miui(x: i32, y: i32, w: i32, h: i32, title: &'static str) -> Button {
        let mut button = Button::new(x, y, w, h, title);
        button.set_frame(FrameType::FlatBox);
        button.visible_focus(false);
        button.set_label_color(Color::White);
        button.set_color(Color::rgb_color(78, 110, 242));
        button.set_selection_color(Color::rgb_color(78 - 40, 110 - 40, 242 - 40));
        button
    }
    fn miui_default() -> Button {
        <Button as MButton>::miui(0,0,0,0,"")
    }

    fn btn_color(&mut self, color: Color) {
        self.set_color(color);
        let (r, g, b) = color.to_rgb();
        self.set_selection_color(Color::rgb_color((r as f32 * 0.8) as u8 , (g as f32  * 0.8) as u8, (b as f32 * 0.8) as u8));
        let mut border = Color::rgb_color((r as f32 * 0.9) as u8 , (g as f32  * 0.9) as u8,  (b as f32 * 0.9) as u8);
        self.draw(move |b| {
            let (x1, x2, y1, y2) = (b.x(), b.x() + b.w(), b.y(), b.y() + b.h() - 1);
            set_draw_color(border);
            draw_line(x1, y1, x2, y1);
            draw_line(x2, y1, x2, y2);
            draw_line(x2, y2, x1, y2);
            draw_line(x1, y2, x1, y1);
            if let Some(mut img) = b.image() {
                if event() == Event::Push {
                    set_draw_color(b.selection_color());
                } else {
                    set_draw_color(b.color());
                }

                draw_rectf(b.x(), b.y(), b.w()+1, b.h());
                let label = b.label();
                let (mut width, height) = draw::measure(label.as_str(), false);
                if width!=0 {
                    img.draw(b.x() + ( b.w()/2 - width/2 - img.w() ) , b.y() + (b.h() / 2 - img.h() / 2), img.w(), img.h());
                }else{
                    img.draw(b.x() + ( b.w()/2 - img.w()/2 ) , b.y() + (b.h() / 2 - img.h() / 2), img.w(), img.h());
                }

                if label.len() > 0 {
                    set_draw_color(b.label_color());
                    draw_text2(label.as_str(), b.x(), b.y(), b.w(), b.h(), Align::Center);
                }
            }
            // if b.set_deimage()
        });
    }
}

pub trait MInput {
    fn miui(x: i32, y: i32, w: i32, h: i32, title: &'static str) -> Input;
}

impl MInput for Input {
    fn miui(x: i32, y: i32, w: i32, h: i32, title: &'static str) -> Input {
        let mut input = Input::new(x, y, w, h, title);
        input.set_frame(FlatBox);
        input.set_color(Color::White);
        input.set_selection_color(global_border_color);
        input.draw(|i| {
            let (x1, x2, y1, y2) = (i.x() - 1, i.x() + i.w(), i.y() - 1, i.y() + i.h() - 1);
            set_draw_color(i.selection_color());
            begin_line();
            draw_line(x1, y1, x2, y1);
            draw_line(x2, y1, x2, y2);
            draw_line(x2, y2, x1, y2);
            draw_line(x1, y2, x1, y1);
            end_line();
        });
        input.redraw();
        input
    }
}

pub trait MInputChoice{
    fn miui(x: i32, y: i32, w: i32, h: i32, title: &'static str)->InputChoice;
    fn miui_default()->InputChoice;
}

impl MInputChoice for InputChoice{
    fn miui(x: i32, y: i32, w: i32, h: i32, title: &'static str) -> InputChoice {
        let mut menu = InputChoice::new(x,y,w,h, title);

        let mut menu_btn = menu.menu_button();
        menu_btn.visible_focus(false);
        menu_btn.set_frame(FrameType::FlatBox);
        menu_btn.set_color(Color::from_rgb(235, 237, 234));
        menu_btn.set_selection_color(Color::White);

        menu.draw(|b|{
            let (x1,x2,y1,y2) = (b.x()-1,b.x()+b.w(),b.y()-1,b.y()+b.h()-1);
            set_draw_color(b.color());
            begin_line();
            draw_line(x1,y1,x2,y1);
            draw_line(x2,y1,x2,y2);
            draw_line(x2,y2,x1,y2);
            draw_line(x1,y2,x1,y1);
            end_line();
        });
        menu.set_frame(FrameType::NoBox);
        menu
    }

    fn miui_default() -> InputChoice {
        <InputChoice as MInputChoice>::miui(0,0,0,0,"")
    }
}

pub trait MFlex{
    fn miui(x: i32, y: i32, w: i32, h: i32, title: &'static str)->Flex;
    fn miui_default()->Flex;
}

impl MFlex for Flex{
    fn miui(x: i32, y: i32, w: i32, h: i32, title: &'static str) -> Flex {
        let mut flex = Flex::new(0, 0, 600, 300, "");
        flex.set_frame(FrameType::FlatBox);
        flex.set_color(Color::White);
        flex
    }

    fn miui_default() -> Flex {
        <Flex as MFlex>::miui(0,0,0,0,"")
    }
}