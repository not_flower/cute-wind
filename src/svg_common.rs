use fltk::enums::Color;
use fltk::image::SvgImage;

pub struct SVG {}

impl SVG {
    fn change_color(svg_data:&str,color_op:Option<Color>)->SvgImage{
        return match color_op {
            None => {
                SvgImage::from_data(svg_data).unwrap()
            }
            Some(color) => {
                let (r,g,b) = color.to_rgb();
                let rgb_str = format!("rgb({}, {}, {})",r,g,b);
                let mut svg_data = svg_data.replace("rgb(0, 0, 0)",rgb_str.as_str());
                SvgImage::from_data(svg_data.as_str()).unwrap()
            }
        }
    }
    //对勾
    pub fn check_mark(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"/>
            </svg>
        "#;

        return SVG::change_color(svg_data,color_op);
    }
    //垃圾桶
    pub fn trash_can(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M16 1.75V3h5.25a.75.75 0 010 1.5H2.75a.75.75 0 010-1.5H8V1.75C8 .784 8.784 0 9.75 0h4.5C15.216 0 16 .784 16 1.75zm-6.5 0a.25.25 0 01.25-.25h4.5a.25.25 0 01.25.25V3h-5V1.75zM4.997 6.178a.75.75 0 10-1.493.144L4.916 20.92a1.75 1.75 0 001.742 1.58h10.684a1.75 1.75 0 001.742-1.581l1.413-14.597a.75.75 0 00-1.494-.144l-1.412 14.596a.25.25 0 01-.249.226H6.658a.25.25 0 01-.249-.226L4.997 6.178zM9.206 7.501a.75.75 0 01.793.705l.5 8.5A.75.75 0 119 16.794l-.5-8.5a.75.75 0 01.705-.793zm6.293.793A.75.75 0 1014 8.206l-.5 8.5a.75.75 0 001.498.088l.5-8.5z"/>
            </svg>
        "#;

        return SVG::change_color(svg_data,color_op);
    }
    //循环箭头
    pub fn cycle_arrow(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M8 2.5a5.487 5.487 0 00-4.131 1.869l1.204 1.204A.25.25 0 014.896 6H1.25A.25.25 0 011 5.75V2.104a.25.25 0 01.427-.177l1.38 1.38A7.001 7.001 0 0114.95 7.16a.75.75 0 11-1.49.178A5.501 5.501 0 008 2.5zM1.705 8.005a.75.75 0 01.834.656 5.501 5.501 0 009.592 2.97l-1.204-1.204a.25.25 0 01.177-.427h3.646a.25.25 0 01.25.25v3.646a.25.25 0 01-.427.177l-1.38-1.38A7.001 7.001 0 011.05 8.84a.75.75 0 01.656-.834z"/>
            </svg>
        "#;
        return SVG::change_color(svg_data,color_op);
    }
    //笔
    pub fn pen(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M11.013 1.427a1.75 1.75 0 012.474 0l1.086 1.086a1.75 1.75 0 010 2.474l-8.61 8.61c-.21.21-.47.364-.756.445l-3.251.93a.75.75 0 01-.927-.928l.929-3.25a1.75 1.75 0 01.445-.758l8.61-8.61zm1.414 1.06a.25.25 0 00-.354 0L10.811 3.75l1.439 1.44 1.263-1.263a.25.25 0 000-.354l-1.086-1.086zM11.189 6.25L9.75 4.81l-6.286 6.287a.25.25 0 00-.064.108l-.558 1.953 1.953-.558a.249.249 0 00.108-.064l6.286-6.286z"/>
            </svg>
        "#;
        return SVG::change_color(svg_data,color_op);
    }
    //眼睛
    pub fn eye(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
            <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M1.679 7.932c.412-.621 1.242-1.75 2.366-2.717C5.175 4.242 6.527 3.5 8 3.5c1.473 0 2.824.742 3.955 1.715 1.124.967 1.954 2.096 2.366 2.717a.119.119 0 010 .136c-.412.621-1.242 1.75-2.366 2.717C10.825 11.758 9.473 12.5 8 12.5c-1.473 0-2.824-.742-3.955-1.715C2.92 9.818 2.09 8.69 1.679 8.068a.119.119 0 010-.136zM8 2c-1.981 0-3.67.992-4.933 2.078C1.797 5.169.88 6.423.43 7.1a1.619 1.619 0 000 1.798c.45.678 1.367 1.932 2.637 3.024C4.329 13.008 6.019 14 8 14c1.981 0 3.67-.992 4.933-2.078 1.27-1.091 2.187-2.345 2.637-3.023a1.619 1.619 0 000-1.798c-.45-.678-1.367-1.932-2.637-3.023C11.671 2.992 9.981 2 8 2zm0 8a2 2 0 100-4 2 2 0 000 4z"/>
           </svg>
        "#;
        return SVG::change_color(svg_data,color_op);
    }
    //清单检测
    pub fn checklist(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M2.5 1.75a.25.25 0 01.25-.25h8.5a.25.25 0 01.25.25v7.736a.75.75 0 101.5 0V1.75A1.75 1.75 0 0011.25 0h-8.5A1.75 1.75 0 001 1.75v11.5c0 .966.784 1.75 1.75 1.75h3.17a.75.75 0 000-1.5H2.75a.25.25 0 01-.25-.25V1.75zM4.75 4a.75.75 0 000 1.5h4.5a.75.75 0 000-1.5h-4.5zM4 7.75A.75.75 0 014.75 7h2a.75.75 0 010 1.5h-2A.75.75 0 014 7.75zm11.774 3.537a.75.75 0 00-1.048-1.074L10.7 14.145 9.281 12.72a.75.75 0 00-1.062 1.058l1.943 1.95a.75.75 0 001.055.008l4.557-4.45z"/>
           </svg>
        "#;
        return SVG::change_color(svg_data,color_op);
    }
    //圆形中间对勾
    pub fn check_circle(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 18 18">
                <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M8 16A8 8 0 108 0a8 8 0 000 16zm3.78-9.72a.75.75 0 00-1.06-1.06L6.75 9.19 5.28 7.72a.75.75 0 00-1.06 1.06l2 2a.75.75 0 001.06 0l4.5-4.5z"/>
           </svg>
        "#;
        return SVG::change_color(svg_data,color_op);
    }
    //放大镜
    pub fn magnifier(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M11.5 7a4.499 4.499 0 11-8.998 0A4.499 4.499 0 0111.5 7zm-.82 4.74a6 6 0 111.06-1.06l3.04 3.04a.75.75 0 11-1.06 1.06l-3.04-3.04z"/>
           </svg>
        "#;
        return SVG::change_color(svg_data,color_op);
    }
    // //方框
    // pub fn box_(color_op:Option<Color>) -> SvgImage {
    //     let svg_data = r#"
    //        <svg width="16" height="16" viewBox="0 0 16 16">
    //           <path fill="rgb(0, 0, 0)" d="M 1 1 L 1 15 L 15 15 L 15 1Z" stroke="rgb(0, 0, 0)" fill="none"/>
    //         </svg>
    //     "#;
    //     return SVG::change_color(svg_data,color_op);
    // }
    //飞机
    pub fn aircraft(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M1.592 2.712L2.38 7.25h4.87a.75.75 0 110 1.5H2.38l-.788 4.538L13.929 8 1.592 2.712zM.989 8L.064 2.68a1.341 1.341 0 011.85-1.462l13.402 5.744a1.13 1.13 0 010 2.076L1.913 14.782a1.341 1.341 0 01-1.85-1.463L.99 8z"/>
           </svg>
        "#;
        return SVG::change_color(svg_data,color_op);
    }
    pub fn right_dir(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M6.22 3.22a.75.75 0 011.06 0l4.25 4.25a.75.75 0 010 1.06l-4.25 4.25a.75.75 0 01-1.06-1.06L9.94 8 6.22 4.28a.75.75 0 010-1.06z"/>
           </svg>
        "#;
        return SVG::change_color(svg_data,color_op);
    }
    pub fn down_dir(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                <path fill="rgb(0, 0, 0)" fill-rule="evenodd" d="M12.78 6.22a.75.75 0 010 1.06l-4.25 4.25a.75.75 0 01-1.06 0L3.22 7.28a.75.75 0 011.06-1.06L8 9.94l3.72-3.72a.75.75 0 011.06 0z"/>
           </svg>
        "#;
        return SVG::change_color(svg_data,color_op);
    }
    pub fn plus_circle(color_op:Option<Color>) -> SvgImage {
        let svg_data = r#"
           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 18 18">
            <path fill="rgb(0, 0, 0)"  fill-rule="evenodd" d="M1.5 8a6.5 6.5 0 1113 0 6.5 6.5 0 01-13 0zM8 0a8 8 0 100 16A8 8 0 008 0zm.75 4.75a.75.75 0 00-1.5 0v2.5h-2.5a.75.75 0 000 1.5h2.5v2.5a.75.75 0 001.5 0v-2.5h2.5a.75.75 0 000-1.5h-2.5v-2.5z"/>
           </svg>
        "#;
        return SVG::change_color(svg_data,color_op);
    }
}