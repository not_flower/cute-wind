#![windows_subsystem = "windows"]

use std::cell::RefCell;
use std::collections::HashMap;
use std::error::Error;
use std::ops::Deref;
use std::rc::Rc;
use std::sync::{Arc, Mutex, MutexGuard};
use std::time::Duration;

use fltk::{app, group, window};
use fltk::app::{Sender, WidgetId};
use fltk::button::{Button, CheckButton};
use fltk::enums::{Align, Color, FrameType};
use fltk::frame::Frame;
use fltk::group::{Flex, FlexType, Group};
use fltk::image::{JpegImage, PngImage};
use fltk::input::{Input, InputType};
use fltk::misc::Tooltip;
use fltk::prelude::{DisplayExt, GroupExt, ImageExt, InputExt, SurfaceDevice, TableExt, WidgetBase, WidgetExt, WindowExt};
use lazy_static::lazy_static;
use rand::Rng;
use redis::{Client, Commands, Connection, ConnectionAddr, ConnectionInfo, ConnectionLike, ErrorKind, RedisConnectionInfo, RedisError, RedisResult};
use sp::storage;
use sp::storage_table;

use MIUI::MGroupInput;

use crate::MIUI::{BTN_CONFIRM, Label, M_Table, MButton, MCheckButton, MDialog, MFlex, MInput, MTable};
use crate::redis_tree::{RedisTreeGroup, RedisTreeItemGroup};
use crate::svg_common::SVG;

mod tree;
mod redis_tree;
mod MIUI;
mod svg_common;
mod table;
mod widget_table;

type Json_Value = serde_json::Value;
type Json_Error = serde_json::Error;
type Json_Map<K, V> = serde_json::Map<K, V>;
type Json_Result<T> = serde_json::Result<T>;


#[derive(storage)]
pub struct storage;

lazy_static! {
    static ref REDIS_CONNECTINFO_ARC:Arc<Mutex<Option<RedisConnectInfo>>> = Arc::new(Mutex::new(Some(RedisConnectInfo::default())));
    static ref SLIDE:Arc<Mutex<Option<(Frame)>>> = Arc::new(Mutex::new(None));
    static ref w_param:Arc<Mutex<(i32,i32,i32,i32)>> = Arc::new(Mutex::new((0,0,0,0)));
    static ref SENDER:Arc<Mutex<Option<Sender<Message>>>> = Arc::new(Mutex::new(None));
}



pub fn center(w: i32, h: i32) -> (i32, i32) {
    (
        (app::screen_size().0 / 2.0) as i32 - w / 2,
        (app::screen_size().1 / 2.0) as i32 - h / 2,
    )
}


fn slide_prompt(txt: &str) {
    let mut bind = w_param.lock().unwrap();
    let (mut x, mut y, w, h) = *bind;
    x = x + w / 2 - 100;
    MDialog::slide_prompt(txt, x, y, 200, h);
}

fn set_redis_connect_info(conn: RedisConnectInfo) {
    let mut bind = REDIS_CONNECTINFO_ARC.lock().unwrap();
    let _ = bind.insert(conn);
}

fn get_redis_connect_info() -> RedisConnectInfo {
    let mut bind = REDIS_CONNECTINFO_ARC.lock().unwrap();
    let mut bind = bind.as_mut().unwrap();
    bind.clone()
}

fn hash_to_string(hash: &HashMap<String, String>) -> String {
    let mut json_str = String::from("{");
    for (k, v) in hash.iter() {
        json_str.push_str("\"");
        json_str.push_str(k.as_str());
        json_str.push_str("\":");
        json_str.push_str("\"");
        json_str.push_str(v.as_str());
        json_str.push_str("\",");
    }
    json_str.remove(json_str.len() - 1);
    json_str.push_str("}");
    json_str
}

fn kv_save_or_add(key: String, val: &str, notice_arc: Rc<RefCell<Notice>>) {
    let mut connect_info = get_redis_connect_info();
    let connect_res = connect_info.connect();

    match connect_res {
        Ok(mut connect) => {
            let redis_type: RedisResult<String> = redis::cmd("TYPE").arg(key.clone()).query(&mut connect);
            let redis_type = redis_type.unwrap();
            match MSG_TYPE::get(redis_type.as_str()) {
                MSG_TYPE::STRING => {
                    let v_res: RedisResult<String> = connect.set(&key, val);
                    if let Ok(v) = v_res {
                        slide_prompt("保存成功");
                    }
                }
                MSG_TYPE::HASH => {
                    let (x, y) = center(600, 300);
                    let mut dialog = MDialog::new(x, y, 600, 300, "新增");
                    let mut hg = Flex::miui(0, 0, 600, 300, "");
                    hg.set_type(FlexType::Column);

                    let field_label = Label::default("Field");
                    let mut field_input = Input::miui(0, 30, 360, 30, "");

                    let value_label = Label::default("Value");
                    let mut value_input = Input::miui(0, 90, 360, 60, "").with_type(InputType::Multiline);

                    hg.set_size(&*field_label, 30);
                    hg.set_size(&*value_label, 30);
                    hg.set_size(&field_input, 30);
                    hg.set_margin(5);
                    hg.end();

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.window().unwrap().hide());

                    let mut confirm = Button::miui_default().with_size(50, 20).with_label("保存");
                    confirm.btn_color(BTN_CONFIRM);
                    let notice_arc = notice_arc.clone();
                    let key = key.clone();
                    confirm.set_callback(move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let filed = field_input.value();
                            let value = value_input.value();
                            //删除hash中的某k
                            let del_res: RedisResult<i32> = connect.hset(&key, filed, value);
                            if let Ok(n) = del_res {
                                if n > 0 {
                                    let notice_msg = NoticeMsg::new(key.to_string().clone());
                                    notice_arc.borrow().send(&"v_content".to_string(), &notice_msg);
                                    slide_prompt("保存成功");
                                }
                            }
                        }

                        b.window().unwrap().hide();
                    });
                    dialog.add_btn(&mut confirm);
                    dialog.show();
                }
                MSG_TYPE::LIST => {
                    let (x, y) = center(600, 300);

                    let mut dialog = MDialog::new(x, y, 600, 300, "保存");
                    let mut hg = Flex::miui(0, 0, 600, 300, "");
                    hg.set_type(FlexType::Column);

                    let value_label = Label::default("Value");
                    let mut value_input = Input::miui(0, 90, 360, 60, "").with_type(InputType::Multiline);
                    hg.set_size(&*value_label, 30);
                    hg.set_margin(5);
                    hg.end();

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.parent().unwrap().parent().unwrap().hide());

                    let mut confirm = Button::miui_default().with_size(50, 20).with_label("保存");
                    confirm.btn_color(BTN_CONFIRM);
                    let notice_arc = notice_arc.clone();
                    let key = key.clone();
                    confirm.set_callback(move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let value = value_input.value();
                            let lset_res: RedisResult<i64> = connect.lpush(&key, value);
                            if let Ok(n) = lset_res {
                                let notice_msg = NoticeMsg::new(key.to_string().clone());
                                notice_arc.borrow().send(&"v_content".to_string(), &notice_msg);
                                slide_prompt("保存成功");
                            }
                        }

                        b.window().unwrap().hide();
                    });
                    dialog.add_btn(&mut confirm);
                    dialog.show();
                }
                MSG_TYPE::SET => {
                    let (x, y) = center(600, 300);

                    let mut dialog = MDialog::new(x, y, 600, 300, "修改");
                    let mut hg = Flex::miui(0, 0, 600, 300, "");
                    hg.set_type(FlexType::Column);


                    let value_label = Label::default("Value");
                    let mut value_input = Input::miui(0, 90, 360, 60, "").with_type(InputType::Multiline);
                    hg.set_size(&*value_label, 30);
                    hg.set_margin(5);
                    hg.end();

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.parent().unwrap().parent().unwrap().hide());

                    let mut confirm = Button::miui_default().with_size(50, 20).with_label("保存");
                    confirm.btn_color(BTN_CONFIRM);
                    let notice_arc = notice_arc.clone();
                    let key = key.clone();
                    confirm.set_callback(move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let value = value_input.value();//修改后的值

                            //先删除，后添加
                            let del_res: RedisResult<i32> = connect.sadd(&key, value);
                            if let Ok(_) = del_res {
                                let notice_msg = NoticeMsg::new(key.to_string().clone());
                                notice_arc.borrow().send(&"v_content".to_string(), &notice_msg);
                                slide_prompt("保存成功");
                            }
                        }

                        b.window().unwrap().hide();
                    });
                    dialog.add_btn(&mut confirm);
                    dialog.show();
                }
                MSG_TYPE::ZSET => {
                    let (x, y) = center(600, 300);

                    let mut dialog = MDialog::new(x, y, 600, 300, "保存");
                    let mut hg = Flex::miui(0, 0, 600, 300, "");
                    hg.set_type(FlexType::Column);

                    let field_label = Label::default("Score");
                    let mut score_input = Input::miui(0, 30, 360, 30, "");


                    let value_label = Label::default("Value");
                    let mut value_input = Input::miui(0, 90, 360, 60, "").with_type(InputType::Multiline);
                    hg.set_size(&*value_label, 30);
                    hg.set_size(&*field_label, 30);
                    hg.set_size(&score_input, 30);
                    hg.set_margin(5);
                    hg.end();

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.parent().unwrap().parent().unwrap().hide());

                    let mut confirm = Button::miui_default().with_size(50, 20).with_label("保存");
                    confirm.btn_color(BTN_CONFIRM);
                    let notice_arc = notice_arc.clone();
                    let key = key.clone();
                    confirm.set_callback(move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let value = value_input.value();//修改后的值
                            let score_value = score_input.value();
                            //先删除，后添加
                            let del_res: RedisResult<i32> = connect.zadd(&key, value.as_str(), score_value.as_str());
                            if let Ok(_) = del_res {
                                let notice_msg = NoticeMsg::new(key.to_string().clone());
                                notice_arc.borrow().send(&"v_content".to_string(), &notice_msg);
                                slide_prompt("保存成功");
                            }
                        }

                        b.window().unwrap().hide();
                    });
                    dialog.add_btn(&mut confirm);
                    dialog.show();
                }
                MSG_TYPE::STREAM => {
                    let (x, y) = center(600, 300);
                    let mut dialog = MDialog::new(x, y, 600, 300, "新增");
                    let mut hg = Flex::miui(0, 0, 600, 300, "");
                    hg.set_type(FlexType::Column);

                    let field_label = Label::default("ID");
                    let mut Id_input = Input::miui(0, 30, 360, 30, "");
                    Id_input.set_value("*");
                    Id_input.deactivate();

                    let value_label = Label::default("Value");
                    let mut value_input = Input::miui(0, 90, 360, 60, "").with_type(InputType::Multiline);
                    hg.set_size(&*value_label, 30);
                    hg.set_size(&*field_label, 30);
                    hg.set_size(&Id_input, 30);
                    hg.set_margin(5);
                    hg.end();

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.window().unwrap().hide());

                    let mut confirm = Button::miui_default().with_size(50, 20).with_label("保存");
                    confirm.btn_color(BTN_CONFIRM);
                    dialog.add_btn(&mut confirm);
                    let notice_arc = notice_arc.clone();
                    let key = key.clone();
                    confirm.set_callback(move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let value = value_input.value();
                            let json_res: Json_Result<Json_Value> = serde_json::from_str(value.as_str());
                            let mut t: Vec<(String, String)> = vec![];
                            match json_res {
                                Ok(j) => {
                                    let mut hash: &Json_Map<String, Json_Value> = j.as_object().unwrap();
                                    for (k, v) in hash.iter() {
                                        match v {
                                            Json_Value::Null => t.push((k.clone(), "".to_string())),
                                            _ => t.push((k.clone(), v.to_string())),
                                        }
                                    }
                                }
                                Err(e) => {
                                    MDialog::prompt("无效的json字符串").show();
                                    return;
                                }
                            }
                            if t.len() == 0 {
                                return;
                            }

                            //先删除，后添加
                            let xadd_res: RedisResult<String> = connect.xadd(&key, "*", &t);
                            if let Ok(_) = xadd_res {
                                let notice_msg = NoticeMsg::new(key.to_string().clone());
                                notice_arc.borrow().send(&"v_content".to_string(), &notice_msg);
                                slide_prompt("保存成功");
                            }
                        }

                        b.window().unwrap().hide();
                    });
                    dialog.show();
                }
                _ => {}
            }
        }
        Err(e) => {
            redis_tree::slide_prompt_red(format!("{}", e));
            return;
        }
    }


    let notice_msg = NoticeMsg::new(key.to_string());
    notice_arc.borrow().send(&"v_content".to_string(), &notice_msg);
}

#[derive(storage_table)]
#[derive(Clone, Debug)]
pub struct RedisConnectInfo {
    id: String,
    address: String,
    password: String,
    connect_name: String,
    port: i32,
    split_tag: String,
    use_name: String,
    sort: i16,
    db: Arc<Mutex<i8>>,
}

impl RedisConnectInfo {
    pub fn default() -> Self {
        Self {
            id: String::from("default"),
            address: String::new(),
            password: String::new(),
            connect_name: String::new(),
            port: 0,
            split_tag: String::new(),
            use_name: String::new(),
            sort: 0,
            db: Arc::new(Mutex::new(0)),
        }
    }
    pub fn connect(&self) -> RedisResult<Connection> {
        let mut info = self.clone();
        if info.address.len() == 0 {
            let e = RedisError::from((ErrorKind::ClientError, "连接信息为空"));
            return RedisResult::Err(e); //();
        }
        let mut password = None;
        if info.password != "" {
            password = Some(info.password);
        }

        let mut username = None;
        if info.use_name != "" {
            username = Some(info.use_name);
        }

        let db = {
            let bind = info.db.lock().unwrap();
            bind.to_owned() as i64
        };

        // println!("password={:?},username={:?},info.address={},info.port={}", password, username, info.address, info.port);
        //todo 不清楚为啥使用localhost连接很慢，先替换成127.0.0.1
        if info.address.trim() == "localhost" {
            info.address = String::from("127.0.0.1");
        }
        let mut c = Client::open(ConnectionInfo {
            addr: ConnectionAddr::Tcp(info.address, info.port as u16),
            redis: RedisConnectionInfo {
                db,
                password,
                username,
            },
        });
        let c = c.unwrap();
        let mut connection = c.get_connection_with_timeout(
            Duration::from_secs(120)
        );
        return connection;
    }
    pub fn to_struct(rows: &Vec<st::Row>) -> Vec<RedisConnectInfo> {
        let mut list: Vec<RedisConnectInfo> = vec![];
        for row in rows {
            list.push(RedisConnectInfo {
                id: row.getv("id").to_string(),
                address: row.getv("address").to_string(),
                password: row.getv("password").to_string(),
                connect_name: row.getv("connect_name").to_string(),
                port: row.getv("port").to_i32(),
                split_tag: row.getv("split_tag").to_string(),
                use_name: row.getv("use_name").to_string(),
                sort: row.getv("sort").to_i16(),
                db: Arc::new(Mutex::new(row.getv("db").to_i8())),
            })
        }
        return list;
    }
    pub fn set_db(&mut self, db: i8) {
        let mut bind = self.db.lock().unwrap();
        *bind = db;
    }
}

#[derive(Clone, Debug)]
pub enum Message {
    Quit,
    FLUSH_LEFT_GROUP,
    Send(String, String),
}

fn connect_window(data: Option<RedisConnectInfo>) {
    let (x, y) = center(600, 240);
    let mut md = MDialog::new(x, y, 600, 240, "新建连接");
    md.begin();

    Frame::new(5, 0, 100, 30, "地址").set_align(Align::Inside | Align::Left);
    let mut addr = Input::miui(20, 30, 250, 30, "");

    Frame::new(5, 60, 100, 30, "密码").set_align(Align::Inside | Align::Left);
    let mut password = Input::miui(20, 90, 250, 30, "");

    Frame::new(5, 120, 100, 30, "连接名称").set_align(Align::Inside | Align::Left);
    let mut connect_name = Input::miui(20, 150, 250, 30, "");

    let mut lab = Frame::new(300, 0, 100, 30, "端口").set_align(Align::Inside | Align::Left);
    let mut port = Input::miui(300, 30, 250, 30, "");

    Frame::new(300, 60, 100, 30, "用户名").set_align(Align::Inside | Align::Left);
    let mut use_name = Input::miui(300, 90, 250, 30, "");

    Frame::new(300, 120, 100, 30, "分隔符").set_align(Align::Inside | Align::Left);
    let mut split_tag = Input::miui(300, 150, 250, 30, "");


    let mut cluster = CheckButton::miui_default().with_size(14, 14).with_pos(20, 200);
    cluster.set_label("Cluster");
    cluster.set_color(Color::Black);

    let mut id = String::new();
    let mut rng = rand::thread_rng();
    let mut id = rng.gen_range(1..=1000000).to_string();
    id.push_str("-");
    id.push_str(rng.gen_range(1..=1000000).to_string().as_str());

    if let Some(d) = data {
        addr.set_value(d.address.as_str());
        password.set_value(d.password.as_str());
        connect_name.set_value(d.connect_name.as_str());
        port.set_value(d.port.to_string().as_str());
        use_name.set_value(d.use_name.as_str());
        split_tag.set_value(d.split_tag.as_str());
        id = d.id;
    }
    md.end();

    let mut cancel = Button::miui(380, 230, 80, 30, "取消");
    cancel.set_callback(move |b| b.window().unwrap().hide());
    md.add_btn(&mut cancel);

    let mut confirm = Button::miui(480, 230, 80, 30, "确定");
    confirm.btn_color(BTN_CONFIRM);
    md.add_btn(&mut confirm);
    confirm.set_callback(move |b| {
        let addr = addr.value();
        let password = password.value();
        let mut connect_name = connect_name.value();

        let split_tag = split_tag.value();
        let port = port.value();
        let use_name = use_name.value();

        let mut db = RedisConnectInfo_db::new();
        let mut row = db.createRow();

        if connect_name.trim().len() == 0 {
            connect_name.clear();
            let connect_name_str = String::new() + addr.as_str() + connect_name.as_str() + " : " + port.as_str();
            connect_name.push_str(connect_name_str.as_str());
        }

        row.insert("id", Value::from_str(id.as_str()));
        row.insert("address", Value::from_str(addr.as_str()));
        row.insert("password", Value::from_str(password.as_str()));
        row.insert("connect_name", Value::from_str(connect_name.as_str()));
        row.insert("port", Value::from_i32(port.parse().unwrap()));
        row.insert("sort", Value::from_i16(1));
        row.insert("split_tag", Value::from_str(split_tag.as_str()));
        row.insert("use_name", Value::from_str(use_name.as_str()));
        row.insert("db", Value::from_i8(0));

        let rows = db.query(|r| r.eq("id", &Value::from_str(id.as_str())));
        if rows.len() == 0 {
            let _ = db.insert(&row);
        } else {
            db.update(&row, |r| r.eq("id", &Value::from_str(id.as_str())));
        }
        flush_left_group();
        b.window().unwrap().hide();
    });

    md.show();
}

pub fn flush_left_group() {
    let mut bind = SENDER.lock().unwrap();
    let mut s = bind.clone().unwrap();
    s.send(Message::FLUSH_LEFT_GROUP);
}

pub fn send_msg(msg: Message) {
    let mut bind = SENDER.lock().unwrap();
    let mut s = bind.clone().unwrap();
    s.send(msg);
}

pub struct MyApp {
    app: app::App,
    r: app::Receiver<Message>,
    notice_arc: Rc<RefCell<Notice>>,
    redisTree: RedisTreeGroup,
}


impl MyApp {
    pub fn new(args: Vec<String>) -> Self {
        let app = app::App::default().with_scheme(app::Scheme::Oxy);
        app::background(221, 221, 221);


        let mut main_win = window::Window::default()
            .with_size(800, 600)
            .center_screen()
            .with_label("【小钻风】一个Redis桌面管理工具")
            ;
        main_win.resize_callback(|win, x, y, w, h| {
            let mut bind = w_param.lock().unwrap();
            *bind = (x, y, w, h);
        });
        Tooltip::set_delay(0.5);
        Tooltip::set_color(Color::White);
        Tooltip::set_margin_height(5);
        Tooltip::set_margin_width(15);


        let content = include_bytes!("assets/title.png");
        let res = PngImage::from_data(content);
        match res {
            Ok(img) => {
                main_win.set_icon(Some(img));
            }
            Err(m) => {
                println!("error={}", m);
            }
        }

        let mut root_group = group::Group::default().with_pos(0, 0).with_size(main_win.w(), main_win.h());
        root_group.set_frame(FrameType::FlatBox);
        root_group.set_color(Color::White);
        let mut col = group::Flex::default().size_of_parent();
        col.set_type(group::FlexType::Row);
        col.set_frame(FrameType::FlatBox);
        col.set_color(Color::from_rgb(210, 210, 210));
        col.set_pad(1);


        let mut left = Group::new(0, 0, 200, 600, "");
        left.set_frame(FrameType::FlatBox);
        left.set_color(Color::from_rgb(244, 244, 244));

        let mut new_connect = Button::miui(12, 5, 150, 30, "新建连接");
        new_connect.set_callback(move |b| connect_window(None));
        let mut plus_circle = SVG::plus_circle(Some(Color::White));
        plus_circle.scale(12, 12, false, false);
        new_connect.btn_color(Color::rgb_color(78, 110, 242));
        new_connect.set_image(Some(plus_circle));

        let mut tree_group = redis_tree::RedisTreeGroup::new(0, 50, 200, 16000);
        tree_group.set_id("RedisTree");
        let mut db = RedisConnectInfo_db::new();
        let l = db.query(|_| true);
        let info_vec = RedisConnectInfo::to_struct(&l);
        let notice = Notice::new();
        let notice_arc = Rc::new(RefCell::new(notice));
        for mut c in info_vec {
            let mut tree_item = tree_group.addItemGroup(&mut c);
            let mut id = String::from(c.connect_name.clone());
            let mut tree = tree_item.get_tree().clone();
            let redis_connect_info = c.clone();
            let mem = Member::new(id, move |m, msg| {
                RedisTreeItemGroup::loadTreeData(&mut tree, &redis_connect_info, 0, false, "");
            });
            let _ = {
                notice_arc.borrow_mut().add(mem);
            };


            tree_item.selected(move |t, mut conn, id, k| {
                set_redis_connect_info(conn.clone());
                send_msg(Message::Send("v_content".to_string(), k.to_string()));
            });
        }

        left.end();
        left.show();


        let mut right = group::Flex::default().with_size(400, 800);
        right.set_frame(FrameType::FlatBox);
        right.set_color(Color::White);
        right.set_type(group::FlexType::Column);
        right.set_margin(5);

        let mut row1 = Group::default().with_pos(200, 0).with_size(400, 30);
        row1.set_color(Color::White);
        let mut input_key = MGroupInput::new(212, 0, 160, 30);
        input_key.set_title(60, "string");
        input_key.set_value("string");
        input_key.set_title_color(Color::from_rgb(236, 236, 236));
        let mut value_input_key = Arc::new(Mutex::new(String::new()));
        input_key.bind_value_arc_mutex(value_input_key.clone());


        let mut submit = Button::default().with_size(20, 20);
        submit.set_image(Some(SVG::check_mark(None)));
        input_key.add(&submit);
        submit.set_frame(FrameType::NoBox);
        submit.set_color(Color::White);
        // input_key.flush();

        let value_input_key_cp = value_input_key.clone();
        let original_value = input_key.original_value();
        let notice_arc_cp = notice_arc.clone();
        //更换key名称
        submit.set_callback(move |b| {
            let mut connect_info = get_redis_connect_info();
            if let Ok(mut connect) = connect_info.connect() {
                let o_key = {
                    let bind = original_value.lock().unwrap();
                    bind.as_str().to_string().clone()
                };
                let n_key = {
                    let bind = value_input_key_cp.lock().unwrap();
                    bind.as_str().to_string().clone()
                };
                let rename_res: RedisResult<String> = connect.rename(o_key, n_key);
                if let Ok(s) = rename_res {
                    if s.as_str() == "OK" {
                        // let notice_arc = notice_arc_cp.borrow_mut();
                        let id = String::from(connect_info.connect_name);
                        let notice_msg = NoticeMsg::new(id.clone());
                        notice_arc_cp.borrow().send(&id, &notice_msg);
                    }
                }
            }
        });
        submit.center_y(&*input_key);

        let mut input_ttl = MGroupInput::new(380, 0, 100, 30); //Input::default().with_pos(500,0).with_size(200,30);
        input_ttl.set_title(40, "ttl");
        input_ttl.set_title_color(Color::from_rgb(236, 236, 236));
        let mut value_input_ttl = Arc::new(Mutex::new(String::new()));
        input_ttl.bind_value_arc_mutex(value_input_ttl.clone());

        let mut submit2 = Button::default().with_size(20, 20);
        let mut svg = SVG::check_mark(None);
        submit2.set_image(Some(SVG::check_mark(None)));
        input_ttl.add(&submit2);
        submit2.set_frame(FrameType::NoBox);
        submit2.set_color(Color::White);


        let original_key = input_key.original_value();
        let original_ttl = input_ttl.original_value();
        let notice_arc_cp = notice_arc.clone();
        submit2.set_callback(move |b| {
            let mut connect_info = get_redis_connect_info();
            if let Ok(mut connect) = connect_info.connect() {
                let key = {
                    let bind = original_key.lock().unwrap();
                    bind.as_str().to_string().clone()
                };

                let ttl_res: Result<i64, _> = {
                    let bind = value_input_ttl.lock().unwrap();
                    bind.as_str().parse()
                };
                if let Err(e) = ttl_res {
                    MDialog::prompt(e.description()).show();
                    return;
                }
                let ttl = ttl_res.unwrap();
                if ttl <= 0 {
                    return MDialog::prompt("过期时间必须>0").show();
                }
                let expire_res: RedisResult<i64> = connect.expire(key.clone(), ttl as usize);

                if let Err(e) = expire_res {
                    let notice_arc = notice_arc_cp.borrow();
                    let notice_msg = NoticeMsg::new(key);
                    notice_arc.send(&"v_content".to_string(), &notice_msg);
                    return;
                }
                slide_prompt("设置完毕");
            }
        });
        submit2.center_y(&*input_ttl);


        let mut remove = Button::miui_default().with_pos(490, 0).with_size(40, 30);

        let mut svg = SVG::trash_can(Some(Color::White));
        svg.scale(16, 16, true, true);
        remove.set_image(Some(svg));
        remove.btn_color(Color::rgb_color(179, 36, 36));

        remove.set_tooltip("删除");
        let value_input_key_cp = value_input_key.clone();
        let notice_arc_cp = notice_arc.clone();

        remove.set_callback(move |b| {
            let mut connect_info = get_redis_connect_info();
            if connect_info.address.len() == 0 {
                redis_tree::slide_prompt_red("连接信息为空".to_string());
                return;
            }

            let value_input_key_cp = value_input_key_cp.clone();
            let notice_arc_cp = notice_arc_cp.clone();
            MDialog::confirm("确认删除该数据?", move |b| {
                let mut connect_info = get_redis_connect_info();
                if let Ok(mut connect) = connect_info.connect() {
                    let mut key = {
                        let bind = value_input_key_cp.lock().unwrap();
                        bind.as_str().clone().to_string()
                    };
                    let del_res: RedisResult<i32> = connect.del(&key);
                    if let Ok(n) = del_res {
                        if n > 0 {
                            let notice_arc = notice_arc_cp.borrow();
                            let id = String::from(connect_info.connect_name);
                            let notice_msg = NoticeMsg::new(key);
                            notice_arc.send(&id, &notice_msg);
                            notice_arc.send(&"v_content".to_string(), &notice_msg);
                            slide_prompt("删除成功");
                        }
                    }
                }
                b.window().unwrap().hide();
            }).show();
        });

        let mut flush = Button::miui_default().with_pos(500, 0).with_size(40, 30);

        flush.set_image(Some(SVG::cycle_arrow(Some(Color::White))));
        // flush.set_frame(FrameType::FlatBox);
        flush.btn_color(Color::rgb_color(36, 179, 36));
        flush.set_tooltip("刷新");
        let value_input_key_cp = value_input_key.clone();
        let notice_arc_cp = notice_arc.clone();
        flush.set_callback(move |b| {
            let mut key = {
                let bind = value_input_key_cp.lock().unwrap();
                bind.as_str().clone().to_string()
            };
            let notice_msg = NoticeMsg::new(key);
            notice_arc_cp.borrow().send(&"v_content".to_string(), &notice_msg);
            let mut p = b.parent().unwrap();
            p.redraw();
        });

        //保存或者新增
        let mut save_or_add = Button::miui_default().with_pos(550, 0).with_size(40, 30);
        // save_or_add.set_frame(FrameType::FlatBox);
        save_or_add.btn_color(Color::rgb_color(78, 110, 242));
        save_or_add.set_image(Some(SVG::checklist(Some(Color::White))));
        save_or_add.set_tooltip("保存或新增行");
        row1.end();


        let mut row2 = Group::default().with_pos(200, 30).with_size(400, 300);
        row2.set_frame(FrameType::FlatBox);
        row2.set_color(Color::White);

        /**----------------remark--------------------*/
        let mut remark_groop = Group::default().with_pos(210, 35).with_size(380, 300);
        let mut remark_frame = Frame::default().with_pos(210, 35).with_size(380, 300);
        remark_frame.set_align(Align::Inside | Align::TopLeft);
        remark_frame.set_label_size(18);
        remark_frame.set_frame(FrameType::FlatBox);
        remark_frame.set_color(MIUI::global_border_color);

        remark_frame.set_label(r#"
这是一个采用用rust+fltk编写的redis管理工具。
它具有体积很小、速度快、内存占用低等优点。

目前这个只能创建单个节点，基础的查询编辑等功能已经完成。
希望能在春节前出一个完整的测试版，年后出一个正式版。

当然，它是开源的，免费的，是超自由的MIT协议。

最后嵌一个赞助码，如果你用的高兴，也希望给作者一丝丝鼓励。
        "#);


        let content = include_bytes!("assets/appreciation_code.jpg");
        let res = JpegImage::from_data(content);
        match res {
            Ok(mut img) => {
                let mut appreciation_code = Frame::default().with_pos(210, 180).with_size(380, 120);
                // img.scale(150,150,false,false);
                appreciation_code.set_image(Some(img));
            }
            Err(m) => {
                println!("error={}", m);
            }
        }
        remark_groop.end();
        /** ---------------string------------------ */
        let mut val_str = Input::miui(210, 35, 380, 295, "")
            .with_type(InputType::Multiline);

        val_str.hide();

        let value_input_key_cp = value_input_key.clone();
        let notice_arc_cp = notice_arc.clone();
        let val_str_cp = val_str.clone();
        save_or_add.set_callback(move |b| {
            let mut key = {
                let bind = value_input_key_cp.lock().unwrap();
                bind.as_str().clone().to_string()
            };
            let notice_arc_cp = notice_arc_cp.clone();
            let val = val_str_cp.value();
            kv_save_or_add(key, val.as_str(), notice_arc_cp);
        });


        let mut trash = SVG::trash_can(Some(Color::Black));
        trash.scale(12, 12, false, false);
        let mut pen = SVG::pen(Some(Color::Black));
        pen.scale(12, 12, false, false);
        let mut eye = SVG::eye(Some(Color::Black));
        eye.scale(12, 12, false, false);


        /** ---------------hash------------------ */
        let mut val_hash_table = MTable::new(210, 35, 400, 300);
        val_hash_table.set_cols(3);
        val_hash_table.edit_col(2);
        val_hash_table.set_headers(vec!["Key", "Value", "操作"]);
        val_hash_table.set_col_width(1, 250);
        val_hash_table.set_col_width(0, 150);
        let hash_trash = trash.clone();
        let hash_pen = pen.clone();
        let val_hash_data: Arc<Mutex<Option<Vec<Vec<String>>>>> = Arc::new(Mutex::new(None));
        val_hash_table.draw_cell(move |t, ctx, datas, row, col, x, y, w, h| {
            let mut txt = String::new();
            if datas.len() == 0 || datas.len() <= row as usize {
                return;
            }
            match datas.get(row as usize) {
                None => {
                    return;
                }
                Some(row_data) => {
                    if row_data.len() <= col as usize {
                        return;
                    }
                    if let Some(mut t) = row_data.get(col as usize) {
                        txt.push_str(t.as_str());
                    }
                }
            }

            if col == 2 {
                txt.clear();
            }
            t.draw_cell_content(txt.as_str(), row, col, x, y, w, h, t.is_selected(row, col));
            if col == 2 {
                t.draw_cell_svg(&mut hash_trash.clone(), x + 5, y + 8, 12, 12);
                t.draw_cell_svg(&mut hash_pen.clone(), x + 30, y + 8, 12, 12);
            }
        });


        let notice_arc_cp = notice_arc.clone();
        let value_input_key_cp = value_input_key.clone();
        val_hash_table.callback(move |t, data| {
            if let Some((r, c, r2, c2)) = t.try_get_selection() {
                if r != r2 || c != c2 {
                    return;
                }
                if c != t.cols() - 1 {
                    return;
                }
                let (x, y, w, h) = t.find_cell(t.callback_context(), r, c).unwrap();
                let (m_x, m_y) = app::event_coords();
                //点击删除
                if m_x > (x + 5) && m_x < (x + 5 + 12) && m_y > y + 8 && m_y < (y + 8 + 12) {
                    let mut v = String::default();
                    {
                        let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                        let mut bind: &Vec<Vec<String>> = bind.as_ref();
                        v.push_str(&bind[r as usize][0usize].as_str())
                    }


                    let (x, y) = center(300, 60);

                    let mut dialog = MDialog::new(x, y, 300, 60, "提示");

                    let mut text = String::from("确定是否删除key:");
                    text.push_str(v.as_str());
                    text.push_str("?");
                    dialog.set_text(text.as_str());

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.parent().unwrap().parent().unwrap().hide());

                    let mut confirm = Button::miui_default().with_size(50, 20).with_label("确定");
                    confirm.btn_color(BTN_CONFIRM);
                    let mut value_input_key = value_input_key_cp.clone();
                    let row = r;
                    let mut table = t.clone();
                    let mut data = data.clone();
                    confirm.set_callback(move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let mut key: String;
                            key = {
                                let mut value_input_key = value_input_key.lock().unwrap();
                                value_input_key.clone()
                            };
                            //删除hash中的某k
                            let del_res: RedisResult<i32> = connect.hdel(&key, v.as_str());
                            if let Ok(n) = del_res {
                                if n > 0 {
                                    let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                                    bind.remove(row as usize);
                                    table.set_rows(bind.len() as i32);
                                    table.redraw();
                                    slide_prompt("删除成功");
                                }
                            }
                        }
                        b.window().unwrap().hide();
                    });
                    dialog.add_btn(&mut confirm);

                    dialog.show();
                }
                //点击修改
                if m_x > (x + 30) && m_x < (x + 30 + 12) && m_y > y + 8 && m_y < (y + 8 + 12) {
                    let (x, y) = center(600, 300);

                    let mut dialog = MDialog::new(x, y, 600, 300, "修改");
                    let mut hg = Flex::miui(0, 0, 600, 300, "");
                    hg.set_type(FlexType::Column);

                    let mut filed = String::default();
                    let mut value = String::default();

                    {
                        let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                        let mut bind: &Vec<Vec<String>> = bind.as_ref();
                        filed.push_str(&bind[r as usize][0usize].as_str());
                        value.push_str(&bind[r as usize][1usize].as_str());
                    }

                    let field_label = Label::default("Field");
                    let mut field_input = Input::miui(0, 30, 360, 30, "");
                    field_input.set_value(&filed);

                    let value_label = Label::default("Value");
                    let mut value_input = Input::miui(0, 90, 360, 60, "").with_type(InputType::Multiline);
                    value_input.set_value(&value);
                    hg.set_size(&*field_label, 30);
                    hg.set_size(&*value_label, 30);
                    hg.set_size(&field_input, 30);
                    hg.set_margin(5);
                    hg.end();

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.parent().unwrap().parent().unwrap().hide());

                    let mut confirm = Button::miui_default().with_size(50, 20).with_label("保存");
                    confirm.btn_color(BTN_CONFIRM);
                    let row = r;
                    let mut table = t.clone();
                    let mut data = data.clone();
                    let mut value_input_key = value_input_key_cp.clone();
                    let notice_arc = notice_arc_cp.clone();
                    confirm.set_callback(move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let mut key: String;
                            key = {
                                let mut value_input_key = value_input_key.lock().unwrap();
                                value_input_key.clone()
                            };

                            //先删除，后添加
                            let del_res: RedisResult<i32> = connect.hdel(&key, filed.as_str());
                            if let Ok(n) = del_res {
                                if n > 0 {
                                    let hset_res: RedisResult<i32> = connect.hset(&key, field_input.value().as_str(), value_input.value().as_str());
                                    if let Ok(n) = hset_res {
                                        if n > 0 {
                                            let notice_msg = NoticeMsg::new(key.clone());
                                            notice_arc.borrow().send(&"v_content".to_string(), &notice_msg);
                                            slide_prompt("更新完成");
                                        }
                                    }
                                }
                            }
                        }

                        b.parent().unwrap().parent().unwrap().hide();
                    });
                    dialog.add_btn(&mut confirm);
                    dialog.show();
                }

                t.unset_selection();
            }
        });
        val_hash_table.hide();

        /** ---------------list------------------ */
        let mut val_list_table = MTable::new(210, 35, 400, 300);
        val_list_table.set_cols(2);
        val_list_table.set_headers(vec!["Value", "操作"]);
        val_list_table.set_col_width(0, 400);
        let hash_trash = trash.clone();
        let hash_pen = pen.clone();
        let val_list_data: Arc<Mutex<Option<Vec<Vec<String>>>>> = Arc::new(Mutex::new(None));
        val_list_table.draw_cell(move |t, ctx, datas, row, col, x, y, w, h| {
            let mut txt = String::new();
            if datas.len() == 0 || datas.len() <= row as usize {
                return;
            }
            match datas.get(row as usize) {
                None => return,
                Some(row_data) => {
                    if row_data.len() <= col as usize {
                        return;
                    }
                    if let Some(mut t) = row_data.get(col as usize) {
                        txt.push_str(t.as_str());
                    }
                }
            }

            if col == 1 {
                txt.clear();
            }
            t.draw_cell_content(txt.as_str(), row, col, x, y, w, h, t.is_selected(row, col));
            if col == 1 {
                t.draw_cell_svg(&mut hash_trash.clone(), x + 5, y + 8, 12, 12);
                t.draw_cell_svg(&mut hash_pen.clone(), x + 30, y + 8, 12, 12);
            }
        });

        let value_input_key_cp = value_input_key.clone();
        let notice_arc_cp = notice_arc.clone();
        val_list_table.callback(move |t, data| {
            if let Some((r, c, r2, c2)) = t.try_get_selection() {
                let (x, y, w, h) = t.find_cell(t.callback_context(), r, c).unwrap();
                let (m_x, m_y) = app::event_coords();
                //点击删除
                if m_x > (x + 5) && m_x < (x + 5 + 12) && m_y > y + 8 && m_y < (y + 8 + 12) {
                    let mut value_input_key_cp = value_input_key_cp.clone();
                    let row = r;
                    let mut table = t.clone();
                    let mut data = data.clone();
                    MDialog::confirm("确定是否删除?", move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let mut key: String;
                            key = {
                                let mut value_input_key = value_input_key_cp.lock().unwrap();
                                value_input_key.clone()
                            };

                            //删除list中的某k
                            let del_res: RedisResult<String> = connect.lset(&key, row as isize, "");

                            if let Ok(_) = del_res {
                                let del_res: RedisResult<i32> = connect.lrem(&key, 1, "");
                                if let Ok(_) = del_res {
                                    let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                                    bind.remove(row as usize);
                                    table.set_rows(bind.len() as i32);
                                    table.redraw();
                                    slide_prompt("删除成功");
                                }
                            }
                        }

                        b.window().unwrap().hide();
                    }).show();
                }
                //点击修改
                if m_x > (x + 30) && m_x < (x + 30 + 12) && m_y > y + 8 && m_y < (y + 8 + 12) {
                    let (x, y) = center(600, 300);

                    let mut dialog = MDialog::new(x, y, 600, 300, "修改");
                    let mut hg = Flex::miui(0, 0, 600, 300, "");
                    hg.set_type(FlexType::Column);

                    let mut value = String::default();
                    {
                        let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                        let mut bind: &Vec<Vec<String>> = bind.as_ref();
                        value.push_str(&bind[r as usize][0usize].as_str());
                    }

                    let value_label = Label::default("Value");
                    let mut value_input = Input::miui(0, 90, 360, 60, "").with_type(InputType::Multiline);
                    value_input.set_value(&value);
                    hg.set_size(&*value_label, 30);
                    hg.set_margin(5);
                    hg.end();

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.parent().unwrap().parent().unwrap().hide());

                    let mut confirm = Button::miui_default().with_size(50, 20).with_label("保存");
                    confirm.btn_color(BTN_CONFIRM);
                    let row = r;
                    let mut value_input_key_cp = value_input_key_cp.clone();
                    let notice_arc = notice_arc_cp.clone();
                    confirm.set_callback(move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let mut key: String;
                            key = {
                                let mut value_input_key = value_input_key_cp.lock().unwrap();
                                value_input_key.clone()
                            };

                            let lset_res: RedisResult<String> = connect.lset(&key, row as isize, value_input.value().as_str());
                            if let Ok(n) = lset_res {
                                let notice_msg = NoticeMsg::new(key.clone());
                                notice_arc.borrow().send(&"v_content".to_string(), &notice_msg);
                                slide_prompt("更新完成");
                            }
                        }

                        b.window().unwrap().hide();
                    });
                    dialog.add_btn(&mut confirm);
                    dialog.show();
                }

                t.unset_selection();
            }
        });
        val_list_table.hide();

        /** ---------------set------------------ */
        let mut val_set_table = MTable::new(210, 35, 400, 300);
        val_set_table.set_cols(2);
        val_set_table.set_headers(vec!["Value", "操作"]);
        val_set_table.set_col_width(0, 400);

        let hash_trash = trash.clone();
        let hash_pen = pen.clone();
        let val_list_data: Arc<Mutex<Option<Vec<Vec<String>>>>> = Arc::new(Mutex::new(None));
        val_set_table.draw_cell(move |t, ctx, datas, row, col, x, y, w, h| {
            let mut txt = String::new();
            if datas.len() == 0 || datas.len() <= row as usize {
                return;
            }
            match datas.get(row as usize) {
                None => return,
                Some(row_data) => {
                    if row_data.len() <= col as usize {
                        return;
                    }
                    if let Some(mut t) = row_data.get(col as usize) {
                        txt.push_str(t.as_str());
                    }
                }
            }

            if col == 1 {
                txt.clear();
            }
            t.draw_cell_content(txt.as_str(), row, col, x, y, w, h, t.is_selected(row, col));
            if col == 1 {
                t.draw_cell_svg(&mut hash_trash.clone(), x + 5, y + 8, 12, 12);
                t.draw_cell_svg(&mut hash_pen.clone(), x + 30, y + 8, 12, 12);
            }
        });

        let value_input_key_cp = value_input_key.clone();
        let notice_arc_cp = notice_arc.clone();
        val_set_table.callback(move |t, data| {
            if let Some((r, c, r2, c2)) = t.try_get_selection() {
                let (x, y, w, h) = t.find_cell(t.callback_context(), r, c).unwrap();
                let (m_x, m_y) = app::event_coords();
                //点击删除
                if m_x > (x + 5) && m_x < (x + 5 + 12) && m_y > y + 8 && m_y < (y + 8 + 12) {
                    let mut value_input_key_cp = value_input_key_cp.clone();
                    let row = r;
                    let mut table = t.clone();
                    let mut data = data.clone();
                    MDialog::confirm("确定是否删除?", move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let key = {
                                let mut value_input_key = value_input_key_cp.lock().unwrap();
                                value_input_key.clone()
                            };
                            let value = {
                                let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                                &bind[row as usize][0].clone()
                            };


                            //删除set中的某k
                            let del_res: RedisResult<i32> = connect.srem(&key, value.as_str());
                            if let Ok(_) = del_res {
                                let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                                bind.remove(row as usize);
                                table.set_rows(bind.len() as i32);
                                table.redraw();
                                slide_prompt("删除成功");
                            }
                        }

                        b.window().unwrap().hide();
                    }).show();
                }
                //点击修改
                if m_x > (x + 30) && m_x < (x + 30 + 12) && m_y > y + 8 && m_y < (y + 8 + 12) {
                    let (x, y) = center(600, 300);

                    let mut dialog = MDialog::new(x, y, 600, 300, "修改");
                    let mut hg = Flex::miui(0, 0, 600, 300, "");
                    hg.set_type(FlexType::Column);

                    let mut value = String::default();
                    {
                        let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                        let mut bind: &Vec<Vec<String>> = bind.as_ref();
                        value.push_str(&bind[r as usize][0usize].as_str());
                    }

                    let value_label = Label::default("Value");
                    let mut value_input = Input::miui(0, 90, 360, 60, "").with_type(InputType::Multiline);
                    value_input.set_value(&value);
                    hg.set_size(&*value_label, 30);
                    hg.set_margin(5);
                    hg.end();

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.parent().unwrap().parent().unwrap().hide());

                    let mut confirm = Button::miui_default().with_size(50, 20).with_label("保存");
                    confirm.btn_color(BTN_CONFIRM);
                    let row = r;
                    let mut value_input_key_cp = value_input_key_cp.clone();
                    let notice_arc = notice_arc_cp.clone();
                    confirm.set_callback(move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let mut key: String;
                            key = {
                                let mut value_input_key = value_input_key_cp.lock().unwrap();
                                value_input_key.clone()
                            };
                            let value = {
                                let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                                &bind[row as usize][0].clone()
                            };//修改前的值

                            let n_value = value_input.value();//修改后的值

                            //先删除，后添加
                            let del_res: RedisResult<i32> = connect.srem(&key, value.as_str());
                            if let Ok(_) = del_res {
                                let sadd_res: RedisResult<i32> = connect.sadd(&key, n_value);
                                if let Ok(_) = sadd_res {
                                    let notice_msg = NoticeMsg::new(key.clone());
                                    notice_arc.borrow().send(&"v_content".to_string(), &notice_msg);
                                    slide_prompt("更新完成");
                                }
                            }
                        }

                        b.window().unwrap().hide();
                    });
                    dialog.add_btn(&mut confirm);
                    dialog.show();
                }

                t.unset_selection();
            }
        });
        val_set_table.hide();

        /** ---------------zset------------------ */
        let mut val_zset_table = MTable::new(210, 35, 400, 300);
        val_zset_table.set_cols(3);
        val_zset_table.set_headers(vec!["Score", "Value", "操作"]);
        val_zset_table.set_col_width(1, 250);
        val_zset_table.set_col_width(0, 150);

        let hash_trash = trash.clone();
        let hash_pen = pen.clone();
        val_zset_table.draw_cell(move |t, ctx, datas, row, col, x, y, w, h| {
            let mut txt = String::new();
            if datas.len() == 0 || datas.len() <= row as usize {
                return;
            }
            match datas.get(row as usize) {
                None => return,
                Some(row_data) => {
                    if row_data.len() <= col as usize {
                        return;
                    }
                    if let Some(mut t) = row_data.get(col as usize) {
                        txt.push_str(t.as_str());
                    }
                }
            }

            if col == 2 {
                txt.clear();
            }
            t.draw_cell_content(txt.as_str(), row, col, x, y, w, h, t.is_selected(row, col));
            if col == 2 {
                t.draw_cell_svg(&mut hash_trash.clone(), x + 5, y + 8, 12, 12);
                t.draw_cell_svg(&mut hash_pen.clone(), x + 30, y + 8, 12, 12);
            }
        });

        let value_input_key_cp = value_input_key.clone();
        let notice_arc_cp = notice_arc.clone();
        val_zset_table.callback(move |t, data| {
            if let Some((r, c, r2, c2)) = t.try_get_selection() {
                let (x, y, w, h) = t.find_cell(t.callback_context(), r, c).unwrap();
                let (m_x, m_y) = app::event_coords();
                //点击删除
                if m_x > (x + 5) && m_x < (x + 5 + 12) && m_y > y + 8 && m_y < (y + 8 + 12) {
                    let mut value_input_key_cp = value_input_key_cp.clone();
                    let row = r;
                    let mut table = t.clone();
                    let mut data = data.clone();
                    MDialog::confirm("确定是否删除?", move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let key = {
                                let mut value_input_key = value_input_key_cp.lock().unwrap();
                                value_input_key.clone()
                            };
                            let value = {
                                let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                                &bind[row as usize][1].clone()
                            };

                            //删除set中的某k
                            let del_res: RedisResult<i32> = connect.zrem(&key, value.as_str());
                            if let Ok(_) = del_res {
                                let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                                bind.remove(row as usize);
                                table.set_rows(bind.len() as i32);
                                table.redraw();
                                slide_prompt("删除成功");
                            }
                        }

                        b.window().unwrap().hide();
                    }).show();
                }
                //点击修改
                if m_x > (x + 30) && m_x < (x + 30 + 12) && m_y > y + 8 && m_y < (y + 8 + 12) {
                    let (x, y) = center(600, 300);

                    let mut dialog = MDialog::new(x, y, 600, 300, "修改");
                    let mut hg = Flex::miui(0, 0, 600, 300, "");
                    hg.set_type(FlexType::Column);

                    let (mut filed, mut value) = {
                        let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                        let mut bind: &Vec<Vec<String>> = bind.as_ref();
                        (&bind[r as usize][0usize].clone(), &bind[r as usize][1usize].clone())
                    };

                    let field_label = Label::default("Score");
                    let mut score_input = Input::miui(0, 30, 360, 30, "");
                    score_input.set_value(&filed);

                    let value_label = Label::default("Value");
                    let mut value_input = Input::miui(0, 90, 360, 60, "").with_type(InputType::Multiline);
                    value_input.set_value(&value);
                    hg.set_size(&*value_label, 30);
                    hg.set_size(&*field_label, 30);
                    hg.set_size(&score_input, 30);
                    hg.set_margin(5);
                    hg.end();

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.parent().unwrap().parent().unwrap().hide());

                    let mut confirm = Button::miui_default().with_size(50, 20).with_label("保存");
                    confirm.btn_color(BTN_CONFIRM);
                    let row = r;
                    let mut value_input_key = value_input_key_cp.clone();
                    let notice_arc = notice_arc_cp.clone();
                    confirm.set_callback(move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let mut key: String;
                            key = {
                                let mut value_input_key = value_input_key.lock().unwrap();
                                value_input_key.clone()
                            };
                            let value = {
                                let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                                &bind[row as usize][1].clone()
                            };//修改前的值

                            let n_value = value_input.value();//修改后的值
                            let score_value = score_input.value();
                            //先删除，后添加
                            let del_res: RedisResult<i32> = connect.zrem(&key, value.as_str());
                            if let Ok(_) = del_res {
                                let zadd_res: RedisResult<i32> = connect.zadd(&key, n_value, score_value);
                                if let Ok(_) = zadd_res {
                                    let notice_msg = NoticeMsg::new(key.clone());
                                    notice_arc.borrow().send(&"v_content".to_string(), &notice_msg);
                                    slide_prompt("更新完成");
                                }
                            }
                        }

                        b.window().unwrap().hide();
                    });
                    dialog.add_btn(&mut confirm);
                    dialog.show();
                }

                t.unset_selection();
            }
        });
        val_zset_table.hide();

        /** ---------------stream------------------ */
        let mut val_stream_table = MTable::new(210, 35, 400, 300);
        val_stream_table.set_cols(3);
        val_stream_table.set_headers(vec!["ID", "Value", "操作"]);
        val_stream_table.set_col_width(1, 250);
        val_stream_table.set_col_width(0, 150);

        let svg_trash = trash.clone();
        let svg_eye = eye.clone();
        val_stream_table.draw_cell(move |t, ctx, datas, row, col, x, y, w, h| {
            let mut txt = String::new();
            if datas.len() == 0 || datas.len() <= row as usize {
                return;
            }
            match datas.get(row as usize) {
                None => return,
                Some(row_data) => {
                    if row_data.len() <= col as usize {
                        return;
                    }
                    if let Some(mut t) = row_data.get(col as usize) {
                        txt.push_str(t.as_str());
                    }
                }
            }

            if col == 2 {
                txt.clear();
            }
            t.draw_cell_content(txt.as_str(), row, col, x, y, w, h, t.is_selected(row, col));
            if col == 2 {
                t.draw_cell_svg(&mut svg_trash.clone(), x + 5, y + 8, 12, 12);
                t.draw_cell_svg(&mut svg_eye.clone(), x + 30, y + 8, 12, 12);
            }
        });

        let value_input_key_cp = value_input_key.clone();
        let notice_arc_cp = notice_arc.clone();
        val_stream_table.callback(move |t, data| {
            if let Some((r, c, r2, c2)) = t.try_get_selection() {
                let (x, y, w, h) = t.find_cell(t.callback_context(), r, c).unwrap();
                let (m_x, m_y) = app::event_coords();
                //点击删除
                if m_x > (x + 5) && m_x < (x + 5 + 12) && m_y > y + 8 && m_y < (y + 8 + 12) {
                    let mut value_input_key_cp = value_input_key_cp.clone();
                    let row = r;
                    let mut table = t.clone();
                    let mut data = data.clone();
                    MDialog::confirm("确定是否删除?", move |b| {
                        let mut connect_info = get_redis_connect_info();
                        let mut connect_res = connect_info.connect();
                        if let Ok(mut connect) = connect_res {
                            let key = {
                                let mut value_input_key = value_input_key_cp.lock().unwrap();
                                value_input_key.clone()
                            };
                            let value = {
                                let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                                &bind[row as usize][0].clone()
                            };

                            //删除set中的某k
                            let del_res: RedisResult<i32> = connect.xdel(&key, &[value.as_str()]);
                            if let Ok(_) = del_res {
                                let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                                bind.remove(row as usize);
                                table.set_rows(bind.len() as i32);
                                table.redraw();
                                slide_prompt("删除成功");
                            }
                        }

                        b.window().unwrap().hide();
                    }).show();
                }
                //点击查看
                if m_x > (x + 30) && m_x < (x + 30 + 12) && m_y > y + 8 && m_y < (y + 8 + 12) {
                    let (x, y) = center(600, 300);

                    let mut dialog = MDialog::new(x, y, 600, 300, "详情");
                    let mut hg = Flex::miui(0, 0, 600, 300, "");
                    hg.set_type(FlexType::Column);

                    let (mut filed, mut value) = {
                        let mut bind: MutexGuard<Vec<Vec<String>>> = data.lock().unwrap();
                        let mut bind: &Vec<Vec<String>> = bind.as_ref();
                        (&bind[r as usize][0usize].clone(), &bind[r as usize][1usize].clone())
                    };

                    let field_label = Label::default("ID");
                    let mut score_input = Input::miui(0, 30, 360, 30, "");
                    score_input.set_value(&filed);

                    let value_label = Label::default("Value");
                    let mut value_input = Input::miui(0, 90, 360, 60, "").with_type(InputType::Multiline);
                    value_input.set_value(&value);
                    hg.set_size(&*value_label, 30);
                    hg.set_size(&*field_label, 30);
                    hg.set_size(&score_input, 30);
                    hg.set_margin(5);
                    hg.end();

                    let mut cancel = Button::miui_default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.window().unwrap().hide());

                    dialog.show();
                }

                t.unset_selection();
            }
        });
        val_stream_table.hide();

        let input_key_rc = Rc::new(RefCell::from(input_key));
        let input_ttl_rc = Rc::new(RefCell::from(input_ttl));

        let input_key_rc_cp = input_key_rc.clone();
        let input_ttl_rc_cp = input_ttl_rc.clone();

        let mem = Member::new("v_content".to_string(), move |m, s| {
            let mut connect_info = get_redis_connect_info();
            if connect_info.address.len() == 0 {
                redis_tree::slide_prompt_red("连接信息为空".to_string());
                return;
            }
            remark_groop.hide();
            val_str.hide();
            val_str.deactivate();
            val_hash_table.hide();
            val_hash_table.deactivate();
            val_list_table.hide();
            val_list_table.deactivate();
            val_set_table.hide();
            val_set_table.deactivate();
            val_zset_table.hide();
            val_zset_table.deactivate();
            val_stream_table.hide();
            val_stream_table.deactivate();
            val_hash_table.parent().unwrap().redraw();

            val_str.set_value("");
            input_ttl_rc_cp.borrow_mut().set_value("-1");
            // input_ttl.set_value("-1");


            let s = s.clone();
            // input_key.set_value(s.key.as_str());
            input_key_rc_cp.borrow_mut().set_value(s.key.as_str());


            let connect_res = connect_info.connect();

            match connect_res {
                Ok(mut connect) => {
                    let ttl_res: RedisResult<i64> = connect.ttl(s.key.as_str());
                    if let Ok(ttl) = ttl_res {
                        input_ttl_rc_cp.borrow_mut().set_value(ttl.to_string().as_str());
                    }

                    let redis_type: RedisResult<String> = redis::cmd("TYPE").arg(s.key.as_str()).query(&mut connect);
                    let redis_type = redis_type.unwrap();
                    input_key_rc_cp.borrow_mut().set_title(60, redis_type.as_str());
                    match MSG_TYPE::get(redis_type.as_str()) {
                        MSG_TYPE::STRING => {
                            let v_res: RedisResult<String> = connect.get(s.key);
                            if let Ok(v) = v_res {
                                val_str.set_value(v.as_str());
                            }

                            val_str.show();
                            val_str.activate();
                        }
                        MSG_TYPE::HASH => {
                            let v_res: RedisResult<Vec<(String, String)>> = connect.hgetall(s.key);

                            if let Ok(hash) = v_res {
                                val_hash_table.set_rows(hash.len() as i32);
                                let mut data: Vec<Vec<String>> = vec![];
                                for (k, v) in hash {
                                    data.push(vec![k, v, String::new()]);
                                }
                                val_hash_table.clear_data();
                                val_hash_table.set_data(data);
                            }
                            val_hash_table.show();
                            val_hash_table.activate();
                        }
                        MSG_TYPE::LIST => {
                            let v_res: RedisResult<Vec<(String)>> = connect.lrange(s.key, 0, -1);

                            if let Ok(hash) = v_res {
                                val_list_table.set_rows(hash.len() as i32);
                                let mut data: Vec<Vec<String>> = vec![];
                                for v in hash {
                                    data.push(vec![v, String::new()]);
                                }
                                val_list_table.clear_data();
                                val_list_table.set_data(data);
                            }
                            val_list_table.show();
                            val_list_table.activate();
                        }
                        MSG_TYPE::SET => {
                            let v_res: RedisResult<Vec<String>> = connect.smembers(s.key);

                            if let Ok(vec) = v_res {
                                val_set_table.set_rows(vec.len() as i32);
                                let mut data: Vec<Vec<String>> = vec![];
                                for v in vec {
                                    data.push(vec![v, String::new()]);
                                }
                                val_set_table.clear_data();
                                val_set_table.set_data(data);
                            }
                            val_set_table.show();
                            val_set_table.activate();
                        }
                        MSG_TYPE::ZSET => {
                            let v_res: RedisResult<Vec<(String, String)>> = connect.zrange_withscores(s.key, 0, -1);
                            if let Ok(vec) = v_res {
                                val_zset_table.set_rows(vec.len() as i32);
                                let mut data: Vec<Vec<String>> = vec![];
                                for (v, s) in vec {
                                    data.push(vec![s, v, String::new()]);
                                }
                                val_zset_table.clear_data();
                                val_zset_table.set_data(data);
                            }
                            val_zset_table.show();
                            val_zset_table.activate();
                        }
                        MSG_TYPE::STREAM => {
                            let v_res: RedisResult<Vec<HashMap<String, HashMap<String, String>>>> = connect.xrange_all(s.key);
                            if let Ok(vec) = v_res {
                                val_stream_table.set_rows(vec.len() as i32);
                                let mut data: Vec<Vec<String>> = vec![];
                                for hash in vec {
                                    for id in hash.keys() {
                                        if let Some(item_hash) = hash.get(id) {
                                            let mut json_str = hash_to_string(item_hash);
                                            data.push(vec![id.clone(), json_str, String::new()]);
                                        }
                                    }
                                }
                                val_stream_table.clear_data();
                                val_stream_table.set_data(data);
                            }
                            val_stream_table.show();
                            val_stream_table.activate();
                        }
                        _ => {}
                    }
                }
                Err(e) => {
                    redis_tree::slide_prompt_red(e.to_string());
                    return;
                }
            }
        });
        notice_arc.borrow_mut().add(mem);
        row2.end();

        right.set_size(&mut row1, 30);
        right.end();

        col.set_size(&mut left, 200);

        col.end();
        root_group.end();
        main_win.make_resizable(true);
        main_win.end();
        main_win.show();
        let mut input_key_rc_cp = input_key_rc.clone();
        let mut input_ttl_rc_cp = input_ttl_rc.clone();

        let (s, r) = app::channel::<Message>();
        {
            let mut bind = SENDER.lock().unwrap();
            let _ = bind.insert(s);
        }

        let mut tree_group_cp = tree_group.clone();
        main_win.resize_callback(move |r, x, y, w, h| {
            println!("root_group.w()={}", row1.w());
            if w <= 800 || h <= 600 {
                root_group.set_size(800, 600);
            }
            tree_group_cp.set_pos(0, 50);
            tree_group_cp.set_size(200, 16000);

            input_key_rc_cp.borrow_mut().flush();
            input_ttl_rc_cp.borrow_mut().flush();
            remove.set_size(40, 30);

            flush.set_pos(remove.x() + 40 + 10, flush.y());
            flush.set_size(40, 30);


            save_or_add.set_pos(flush.x() + 40 + 10, save_or_add.y());
            save_or_add.set_size(40, 30);

            new_connect.set_size(150, 30);

            root_group.redraw();
        });
        main_win.resize(main_win.x(), main_win.y(), main_win.w(), main_win.h());
        Self {
            app,
            r,
            notice_arc,
            redisTree: tree_group,
        }
    }
    pub fn launch(&mut self) {
        while self.app.wait() {
            use Message::*;
            if let Some(msg) = self.r.recv() {
                match msg {
                    Quit => {}
                    Send(id, key) => {
                        let n = self.notice_arc.borrow_mut();
                        n.send(&id, &NoticeMsg::new(key))
                    }
                    FLUSH_LEFT_GROUP => {
                        println!("-----------");
                        let mut other_group: Group = app::widget_from_id("RedisTree").unwrap();
                        other_group.hide();
                        RedisTreeGroup::group_clearItem(&mut other_group);
                        other_group.show();
                        let mut db = RedisConnectInfo_db::new();
                        let l = db.query(|_| true);
                        let info_vec = RedisConnectInfo::to_struct(&l);
                        for mut c in info_vec {
                            // println!("c={:?}",c);
                            let mut tree_item = self.redisTree.addItemGroup(&mut c);
                            let mut id = String::from(c.connect_name.clone());
                            let mut tree = tree_item.get_tree().clone();
                            let redis_connect_info = c.clone();
                            let mem = Member::new(id, move |m, msg| {
                                RedisTreeItemGroup::loadTreeData(&mut tree, &redis_connect_info, 0, false, "");
                            });
                            self.notice_arc.borrow_mut().add(mem);
                            // let mut tree_item = RedisTree::group_addItem(&mut other_group, &mut c);
                            tree_item.selected(move |t, mut conn, id, k| {
                                set_redis_connect_info(conn.clone());
                                send_msg(Message::Send("v_content".to_string(), k.to_string()));
                            });
                        }
                    }
                    _ => {}
                }
            }
        }
    }
}


pub struct Member {
    id: String,
    f: Arc<Mutex<dyn FnMut(&Member, &NoticeMsg)>>,
}

impl Member {
    fn new<F>(id: String, f: F) -> Self where F: FnMut(&Member, &NoticeMsg) + 'static {
        Self {
            id,
            f: Arc::new(Mutex::new(f)),
        }
    }

    fn id(&self) -> String {
        self.id.clone()
    }
}

#[derive(Clone, Debug)]
enum MSG_TYPE {
    STRING,
    HASH,
    LIST,
    SET,
    ZSET,
    STREAM,
    NONE,
}

impl MSG_TYPE {
    pub fn get(t: &str) -> MSG_TYPE {
        match t {
            "string" => MSG_TYPE::STRING,
            "hash" => MSG_TYPE::HASH,
            "list" => MSG_TYPE::LIST,
            "set" => MSG_TYPE::SET,
            "zset" => MSG_TYPE::ZSET,
            "stream" => MSG_TYPE::STREAM,
            "none" => MSG_TYPE::NONE,
            _ => MSG_TYPE::NONE
        }
    }
    pub fn name_lowercase(&self) -> String {
        match self {
            MSG_TYPE::STRING => "string",
            MSG_TYPE::HASH => "hash",
            MSG_TYPE::LIST => "list",
            MSG_TYPE::SET => "set",
            MSG_TYPE::ZSET => "zset",
            MSG_TYPE::STREAM => "stream",
            MSG_TYPE::NONE => "none",
            _ => "none"
        }.to_string()
    }
    pub fn types(&self) -> String {
        match self {
            MSG_TYPE::STRING => "string",
            MSG_TYPE::HASH => "hash",
            MSG_TYPE::LIST => "list",
            MSG_TYPE::SET => "set",
            MSG_TYPE::ZSET => "zset",
            MSG_TYPE::STREAM => "stream",
            MSG_TYPE::NONE => "none",
            _ => "none"
        }.to_string()
    }
    pub fn get_key_types() -> Vec<String> {
        return vec![
            "string".to_string(),
            "hash".to_string(),
            "list".to_string(),
            "set".to_string(),
            "zset".to_string(),
            "stream".to_string(),
        ];
    }
}

#[derive(Clone)]
pub struct NoticeMsg {
    key: String,
}

impl NoticeMsg {
    pub fn new(key: String) -> Self {
        Self {
            key,
        }
    }
}

pub struct Notice {
    group: HashMap<String, Member>,
}

impl Notice {
    pub fn new() -> Self {
        Self {
            group: HashMap::new()
        }
    }
    pub fn add(&mut self, m: Member) {
        self.group.insert(m.id.clone(), m);
    }
    pub fn send(&self, target_id: &String, msg: &NoticeMsg) {
        let v_op = self.group.get(target_id);
        match v_op {
            None => {}
            Some(m) => {
                let _ = { m.f.lock().unwrap()(m, msg) };
            }
        }
    }
}

fn main() {
    let args: Vec<_> = std::env::args().collect();
    let mut app = MyApp::new(args);
    app.launch();
}
