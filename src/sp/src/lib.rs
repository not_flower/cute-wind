use proc_macro::{Ident, TokenStream};
use quote::quote;
use syn::spanned::Spanned;
use std::ops::{Deref, DerefMut};
use proc_macro2::Span;
use syn::ext::IdentExt;


#[proc_macro_derive(storage_table)]
pub fn derive(input: TokenStream) -> TokenStream {
    let st = syn::parse_macro_input!(input as syn::DeriveInput);
    match do_expand(&st) {
        Ok(token_stream) => token_stream.into(),
        Err(e) => e.to_compile_error().into()
    }
}

fn do_expand(st: &syn::DeriveInput) -> syn::Result<proc_macro2::TokenStream> {
    let db_struct = generate_db_struct(&st)?;
    let ret = quote::quote!(
        #db_struct
    );
    syn::Result::Ok(ret)
}

fn generate_db_struct(st: &syn::DeriveInput) -> syn::Result<proc_macro2::TokenStream> {
    let struct_ident = &st.ident;
    let struct_name_literal = st.ident.to_string();
    let builder_name_literal = format!("{}_db", struct_name_literal);
    let builder_name_ident = syn::Ident::new(&builder_name_literal, st.span());
    let db_name = format!("{}.db", struct_name_literal);
    let db_column = generate_db_column(&st)?;
    let db_column_filed = generate_column_filed(&st)?;

    let ret = quote::quote!(

        pub struct #builder_name_ident{
            table: Table,
        }

        impl #builder_name_ident {

            pub fn new() -> Self {
                let columns = vec![
                    #db_column
                ];


                let mut path = #db_name;
                let mut table = Table::init(&path, columns);
                Self {
                    table
                }
            }

            // pub fn to_struct(rows:&Vec<st::Row>)->Vec<#struct_ident>{
            //     let mut list:Vec<#struct_ident> = vec![];
            //     for row in rows{
            //         list.push(
            //             #struct_ident{
            //                 #db_column_filed
            //             }
            //         )
            //     }
            //     return list;
            // }

        }

        impl std::ops::Deref for  #builder_name_ident {
            type Target = Table;
            fn deref(&self) -> &Self::Target {
                &self.table
            }
        }

        impl std::ops::DerefMut for #builder_name_ident {
            fn deref_mut(&mut self) -> &mut Self::Target {
                &mut self.table
            }
        }
    );
    syn::Result::Ok(ret)
}

type StructFields = syn::punctuated::Punctuated<syn::Field, syn::Token![,]>;
fn get_fields_from_derive_input(st:&syn::DeriveInput)->syn::Result<StructFields>{
    if let syn::Data::Struct(syn::DataStruct{
                                 fields:syn::Fields::Named(syn::FieldsNamed{ref named,..}),
                                 .. }) = st.data{
        return Ok(named.clone());
    };
    Err(syn::Error::new_spanned(st,"自定义错误"))
}

fn generate_db_column(st: &syn::DeriveInput)-> syn::Result<proc_macro2::TokenStream>{
    let fields = get_fields_from_derive_input(st)?;
    let idents:Vec<_> = fields.iter().map(|f|&f.ident).collect();
    let types:Vec<_> = fields.iter().map(|f|&f.ty).collect();
    let mut final_tokenstream = proc_macro2::TokenStream::new();

    for (ident,ty) in idents.iter().zip(types.iter()) {
        if let syn::Type::Path(syn::TypePath{path: syn::Path{segments,..},..})
            = ty {
            if let Some(seg) = segments.last() {

                let ident_str = ident.clone().as_ref().unwrap().to_string();
                let tokenstream_pice = match seg.ident.to_string().as_str() {
                    "i8"=>quote::quote!(Column::new(#ident_str,Type::I8),),
                    "i16"=>quote::quote!(Column::new(#ident_str,Type::I16),),
                    "i32"=>quote::quote!(Column::new(#ident_str,Type::I32),),
                    "i64"=>quote::quote!(Column::new(#ident_str,Type::I64),),
                    "u8"=>quote::quote!(Column::new(#ident_str,Type::U8),),
                    "u16"=>quote::quote!(Column::new(#ident_str,Type::U16),),
                    "u32"=>quote::quote!(Column::new(#ident_str,Type::U32),),
                    "u64"=>quote::quote!(Column::new(#ident_str,Type::U64),),
                    "f32"=>quote::quote!(Column::new(#ident_str,Type::Float),),
                    "f64"=>quote::quote!(Column::new(#ident_str,Type::Double),),
                    "str"=>quote::quote!(Column::new(#ident_str,Type::Char(100)),),//暂时默认100
                    "String"=>quote::quote!(Column::new(#ident_str,Type::Char(100)),),
                    _=>quote::quote!(Column::new(#ident_str,Type::I8),),
                };
                final_tokenstream.extend(tokenstream_pice);
            }
        }
    }
    syn::Result::Ok(final_tokenstream)
}

fn generate_column_filed(st: &syn::DeriveInput)-> syn::Result<proc_macro2::TokenStream> {
    let fields = get_fields_from_derive_input(st)?;
    let idents:Vec<_> = fields.iter().map(|f|&f.ident).collect();
    let types:Vec<_> = fields.iter().map(|f|&f.ty).collect();
    let mut final_tokenstream = proc_macro2::TokenStream::new();

    for (ident,ty) in idents.iter().zip(types.iter()) {
        if let syn::Type::Path(syn::TypePath{path: syn::Path{segments,..},..})
            = ty {
            if let Some(seg) = segments.last() {

                let ident_str = ident.clone().as_ref().unwrap().to_string();

                //eprintln!("====ident_str={}",ident_str);
                let tokenstream_pice = match seg.ident.to_string().as_str() {
                    "i8"=>quote::quote!(#ident:row.getv( #ident_str ).to_i8(),),
                   "i16"=>quote::quote!(#ident:row.getv( #ident_str ).to_i16(),),
                    "i32"=>quote::quote!(#ident:row.getv( #ident_str ).to_i32(),),
                    "i64"=>quote::quote!(#ident:row.getv( #ident_str ).to_i64(),),
                    "u8"=>quote::quote!(#ident:row.getv( #ident_str ).to_u8(),),
                    "u16"=>quote::quote!(#ident:row.getv( #ident_str ).to_u16(),),
                    "u32"=>quote::quote!(#ident:row.getv( #ident_str ).to_u32(),),
                    "u64"=>quote::quote!(#ident:row.getv( #ident_str ).to_u64(),),
                    "f32"=>quote::quote!(#ident:row.getv( #ident_str ).to_f32(),),
                    "f64"=>quote::quote!(#ident:row.getv( #ident_str ).to_f64(),),
                    "str"=>quote::quote!(#ident:row.getv( #ident_str ).to_string(),),//暂时默认100to_string
                    "String"=>quote::quote!(#ident:row.getv( #ident_str ).to_string(),),
                    _=>quote::quote!(#ident:row.getv( #ident_str ).to_string(),),
                };
                final_tokenstream.extend(tokenstream_pice);
            }
        }
    }
    eprintln!("======final_tokenstream={}",final_tokenstream);
    syn::Result::Ok(final_tokenstream)
}

#[proc_macro_derive(storage)]
pub fn derive2(input: TokenStream) -> TokenStream {
    quote::quote!(
        use st::{Table,Type,Value,Column};
        pub mod st{

            use std::collections::HashMap;
            use std::fs::OpenOptions;
            use std::io::{Read, Seek, SeekFrom, Write};
            pub struct Value {
                data: Vec<u8>,
            }

            impl Value {
                pub fn new() -> Self {
                    Self { data: vec![] }
                }
                pub fn from_vec(v: Vec<u8>) -> Self {
                    Self { data: v }
                }
                pub fn from_str(str: &str) -> Self {
                    Self {
                        data: Vec::from(str.as_bytes()),
                    }
                }
                pub fn from_i8(v: i8) -> Self {
                    Self {
                        data: Vec::from(v.to_le_bytes()),
                    }
                }
                pub fn from_i16(v: i16) -> Self {
                    Self {
                        data: Vec::from(v.to_le_bytes()),
                    }
                }
                pub fn from_i32(v: i32) -> Self {
                    Self {
                        data: Vec::from(v.to_le_bytes()),
                    }
                }
                pub fn from_i64(v: i64) -> Self {
                    Self {
                        data: Vec::from(v.to_le_bytes()),
                    }
                }
                pub fn from_bool(v: bool) -> Self {
                    if v {
                        return Self {
                            data: Vec::from(0u8.to_le_bytes()),
                        };
                    }
                    return Self {
                        data: Vec::from(1u8.to_le_bytes()),
                    };
                }
                pub fn from_f32(v: f32) -> Self {
                    Self {
                        data: Vec::from(v.to_le_bytes()),
                    }
                }
                pub fn from_f64(v: f64) -> Self {
                    Self {
                        data: Vec::from(v.to_le_bytes()),
                    }
                }

                pub fn to_string(&self) -> String {
                    let mut v = Vec::new();
                    v.extend_from_slice(&self.data.clone());
                    // println!("{:?}", v);
                    let s = String::from_utf8(v);
                    match s {
                        Ok(str) => str,
                        Err(mess) => mess.to_string(),
                    }
                }
                pub fn to_bool(&self) -> bool {
                    self.data[0] == 0u8
                }
                pub fn to_i8(&self) -> i8 {
                    let mut arr = [0; 1];
                    arr.copy_from_slice(&self.data[0..1]);
                    i8::from_le_bytes(arr)
                }
                pub fn to_i16(&self) -> i16 {
                    let mut arr = [0; 2];
                    arr.copy_from_slice(&self.data[0..2]);
                    i16::from_le_bytes(arr)
                }
                pub fn to_i32(&self) -> i32 {
                    let mut arr = [0; 4];
                    arr.copy_from_slice(&self.data[0..4]);
                    i32::from_le_bytes(arr)
                }
                pub fn to_i64(&self) -> i64 {
                    let mut arr = [0; 8];
                    arr.copy_from_slice(&self.data[0..8]);
                    i64::from_le_bytes(arr)
                }
                pub fn to_u8(&self) -> u8 {
                    let mut arr = [0; 1];
                    arr.copy_from_slice(&self.data[0..1]);
                    u8::from_le_bytes(arr)
                }
                pub fn to_u16(&self) -> u16 {
                    let mut arr = [0; 2];
                    arr.copy_from_slice(&self.data[0..2]);
                    u16::from_le_bytes(arr)
                }
                pub fn to_u32(&self) -> u32 {
                    let mut arr = [0; 4];
                    arr.copy_from_slice(&self.data[0..4]);
                    u32::from_le_bytes(arr)
                }
                pub fn to_u64(&self) -> u64 {
                    let mut arr = [0; 8];
                    arr.copy_from_slice(&self.data[0..8]);
                    u64::from_le_bytes(arr)
                }
                pub fn to_f32(&self) -> f32 {
                    let mut arr = [0; 4];
                    arr.copy_from_slice(&self.data[0..4]);
                    f32::from_le_bytes(arr)
                }
                pub fn to_f64(&self) -> f64 {
                    let mut arr = [0; 8];
                    arr.copy_from_slice(&self.data[0..8]);
                    f64::from_le_bytes(arr)
                }
                pub fn len(&self) -> usize {
                    self.data.len()
                }
                //相等
                pub fn eq(&self, v: &Value) -> bool {
                    self.data == v.data
                }
                //小于
                pub fn lt(&self, v: &Value) -> bool {
                    if self.data.len() != v.data.len() {
                        return self.data.len() < v.data.len();
                    }
                    return self.data < v.data;
                }
                //小于等于
                pub fn le(&self, v: &Value) -> bool {
                    if self.data.len() != v.data.len() {
                        return self.data.len() <= v.data.len();
                    }
                    return self.data <= v.data;
                }
                //大于
                pub fn gt(&self, v: &Value) -> bool {
                    if self.data.len() != v.data.len() {
                        return self.data.len() > v.data.len();
                    }
                    return self.data > v.data;
                }
                //大于等于
                pub fn ge(&self, v: &Value) -> bool {
                    if self.data.len() != v.data.len() {
                        return self.data.len() >= v.data.len();
                    }
                    return self.data >= v.data;
                }

                pub fn is_null(&self) -> bool {
                    self.data.len() == 0
                }
                pub fn is_not_null(&self) -> bool {
                    self.data.len() != 0
                }
            }

            #[derive(Debug)]
            pub enum Type {
                Char(i32),
                I8,
                I16,
                I32,
                I64,
                U8,
                U16,
                U32,
                U64,
                Float,
                Double,
            }

            pub struct Table {
                columns: Vec<Column>,
                path: &'static str,
                len: i32,
            }

            impl Table {
                pub fn init(path: &'static str, columns: Vec<Column>) -> Self {
                    let mut len = 0i32;
                    for col in columns.iter() {
                        match col.columnType {
                            Type::I8 => len += 1,
                            Type::I16 => len += 2,
                            Type::I32 => len += 4,
                            Type::I64 => len += 4,
                            Type::U8 => len += 1,
                            Type::U16 => len += 2,
                            Type::U32 => len += 4,
                            Type::U64 => len += 4,
                            Type::Float => len += 4,
                            Type::Double => len += 8,
                            Type::Char(s) => {
                                len += (s * 4) //按照最多来设计
                            }
                        }
                    }
                    OpenOptions::new()
                        .create(true)
                        .write(true)
                        .open(path)
                        .expect("无法打开或者创建文件");
                    Self { columns, path, len }
                }
            }

            impl Table {
                pub fn insert(&self, row: &Row) -> Result<&str, &str> {
                    if *self.path == *row.tag {
                        let mut buf = vec![0u8; (self.len * 2 + 3) as usize];

                        let mut index = 1usize;
                        for col in self.columns.iter() {
                            match row.get(col.name) {
                                None => {
                                    // let mut s = " ".repeat(col.len as usize);
                                    // buf.push_str(&*s);
                                }
                                Some(v) => {
                                    // println!(
                                    //     "col.name={},v={:?},v.len()={},col.len()={}",
                                    //     col.name,
                                    //     v.data,
                                    //     v.len(),
                                    //     col.len()
                                    // );
                                    if v.len() > col.len() {
                                        panic!("字段的值{},超范围len={}", col.name, col.len());
                                    }
                                    match col.columnType {
                                        Type::Char(_) => {
                                            let mut charData = vec![0u8; 2 + *&v.data.len()];
                                            charData[2..(2 + *&v.data.len())].copy_from_slice(&v.data);
                                            // println!("{:?}", charData);
                                            let bytes = (*&v.data.len() as u16).to_le_bytes();
                                            //存储字符串长度
                                            charData[0] = bytes[0];
                                            charData[1] = bytes[1];
                                            buf[index..(index + v.len() + 2)].copy_from_slice(&*charData);
                                            index += (v.len() + 2);
                                        }
                                        _ => {
                                            buf[index..(index + v.len())].copy_from_slice(&v.data);
                                            index += &v.data.len();
                                        }
                                    }
                                }
                            }
                        }

                        let mut file = OpenOptions::new()
                            .create(true)
                            .read(true)
                            .write(true)
                            .open(self.path)
                            .unwrap();

                        let len = buf.len();
                        let pos = file.seek(SeekFrom::End(0)).unwrap();
                        // println!("file.write={:?}", buf);
                        // // 写入数据
                        return match file.write(&*buf) {
                            Ok(_) => {
                                file.sync_all().unwrap();
                                Ok("成功")
                            }
                            Err(_) => {
                                // let mut tail = vec![0; len];
                                // file.write_all(&tail).unwrap();
                                // println!("失败");
                                Err("失败")
                            }
                        };
                    }
                    Err("不是该表的数据")
                }

                fn load<F, T1>(&self, Fn: F, t1: &mut T1)
                    where
                        F: Fn(&mut T1, u64, u8, u8, Row),
                {
                    let mut file = OpenOptions::new().read(true).open(self.path).unwrap();
                    let meta = file.metadata().unwrap();

                    let mut u8Len = (meta.len() / 8) as usize;
                    //let mut res:Vec<Row> = vec![];
                    while u8Len >= 0 {
                        let mut buf = vec![0u8; (self.len * 2 + 3) as usize];
                        let pos = file.seek(SeekFrom::Current(0)).unwrap();
                        let bytes_read = file.read(&mut *buf).unwrap();
                        if bytes_read == 0 {
                            break;
                        }
                        if buf[0] != 0u8 {
                            continue;
                        }
                        //判断取第一个区域的数据还是取第二个区域的数据。
                        let mut index = 1usize;
                        if buf[self.len as usize] < buf[bytes_read - 1]
                            || (buf[bytes_read - 1] == 255 && buf[self.len as usize] == 0)
                        {
                            index = self.len as usize
                        }

                        let mut rowData = self.createRow();
                        for col in &self.columns {
                            match col.columnType {
                                Type::Char(_) => {
                                    let colBuf = &buf[index..(index + col.len() + 2)];
                                    //前两个位置是字符串实际长度
                                    let len = ((colBuf[1] as u16) << 8) | (colBuf[0] as u16);
                                    //println!("query colBuf={:?},len={}", colBuf, len);
                                    let mut buffer = vec![0u8; len as usize];
                                    buffer.copy_from_slice(&buf[(index + 2)..(index + 2 + len as usize)]);
                                    index += (2 + buffer.len());
                                    let co = buffer.clone();
                                    rowData.insert(col.name, Value::from_vec(buffer));
                                }
                                _ => {
                                    let colBuf = &buf[index..(index + col.len())];
                                    rowData.insert(col.name, Value::from_vec(Vec::from(colBuf)));
                                    index += col.len();
                                }
                            }
                        }
                        Fn(
                            t1,
                            pos,
                            buf[self.len as usize],
                            buf[bytes_read - 1],
                            rowData,
                        );

                        u8Len -= 1;
                    }
                }
                pub fn query<F>(&self, FN: F) -> Vec<Row>
                    where
                        F: Fn(&Row) -> bool,
                {
                    let mut res = vec![];
                    self.load(
                        |vec, pos, end1, end2, row| {
                            if FN(&row) {
                                vec.push(row);
                            }
                        },
                        &mut res,
                    );
                    res
                }

                // pub fn query<F>(&self, FN: F) -> Vec<Row> where F: Fn(&Row) -> bool {
                //     let mut file = OpenOptions::new()
                //         .read(true)
                //         .open(self.path).unwrap();
                //     let meta = file.metadata().unwrap();
                //
                //     let mut u8Len = (meta.len() / 8) as usize;
                //     let mut res: Vec<Row> = vec![];
                //     while u8Len >= 0 {
                //         let mut buf = vec![0u8; (self.len * 2 + 3) as usize];
                //         let pos = file.seek(SeekFrom::Current(0)).unwrap();
                //         let bytes_read = file.read(&mut *buf).unwrap();
                //         if bytes_read == 0 {
                //             break;
                //         }
                //         if buf[0] != 0u8 {
                //             continue;
                //         }
                //         //判断取第一个区域的数据还是取第二个区域的数据。
                //         let mut index = 1usize;
                //         if buf[self.len as usize] < buf[bytes_read - 1] || (buf[bytes_read - 1] == 255 && buf[self.len as usize] == 0) {
                //             index = self.len as usize
                //         }
                //
                //         let mut rowData = self.createRow();
                //         for col in &self.columns {
                //             match col.columnType {
                //                 Type::Char(_) => {
                //                     let colBuf = &buf[index..(index + col.len() + 2)];
                //                     //前两个位置是字符串实际长度
                //                     let len = ((colBuf[1] as u16) << 8) | (colBuf[0] as u16);
                //                     //println!("query colBuf={:?},len={}", colBuf, len);
                //                     let mut buffer = vec![0u8; len as usize];
                //                     buffer.copy_from_slice(&buf[(index + 2)..(index + 2 + len as usize)]);
                //                     index += (2 + buffer.len());
                //                     let co = buffer.clone();
                //                     rowData.insert(col.name, Value::from_vec(buffer));
                //                 }
                //                 _ => {
                //                     let colBuf = &buf[index..(index + col.len())];
                //                     rowData.insert(col.name, Value::from_vec(Vec::from(colBuf)));
                //                     index += col.len();
                //                 }
                //             }
                //         }
                //
                //         if FN(&rowData) {
                //             res.push(rowData);
                //         }
                //
                //         u8Len -= 1;
                //     }
                //     res
                // }

                pub fn delete<F>(&self, FN: F)
                    where
                        F: Fn(&Row) -> bool,
                {
                    let mut file = OpenOptions::new().write(true).open(self.path).unwrap();
                    self.load(
                        |file, pos, end1, end2, r| {
                            if FN(&r) {
                                println!("准备删除数据");
                                let _ = file.seek(SeekFrom::Start(pos));
                                let delTag = [1u8];
                                file.write(&delTag).expect("write 删除失败");
                            }
                        },
                        &mut file,
                    );
                    file.sync_all().expect("sync_all 删除失败");
                }

                pub fn update<F>(&self, newRow: &Row, FN: F)
                    where
                        F: Fn(&Row) -> bool,
                {
                    let mut file = OpenOptions::new().write(true).open(self.path).unwrap();

                    self.load(
                        |file, pos, end1, end2, mut row| {
                            println!("开始修改");
                            if FN(&row) {
                                for col in self.columns.iter() {
                                    match newRow.get(col.name) {
                                        None => {}
                                        Some(v) => {
                                            if v.data.len() != 0 {
                                                row.insert(col.name, Value::from_vec(v.data.clone()));
                                            }
                                        }
                                    }
                                }

                                let mut buf = vec![0u8; (self.len + 1) as usize];
                                let mut index = 1usize;
                                for col in self.columns.iter() {
                                    match row.get(col.name) {
                                        None => {}
                                        Some(v) => {
                                            // println!(
                                            //     "col.name={},v={:?},v.len()={},col.len()={}",
                                            //     col.name,
                                            //     v.data,
                                            //     v.len(),
                                            //     col.len()
                                            // );
                                            if v.len() > col.len() {
                                                panic!("字段的值{},超范围len={}", col.name, col.len());
                                            }
                                            match col.columnType {
                                                Type::Char(_) => {
                                                    let mut charData = vec![0u8; 2 + *&v.data.len()];
                                                    charData[2..(2 + *&v.data.len())].copy_from_slice(&v.data);
                                                    // println!("{:?}", charData);
                                                    let bytes = (*&v.data.len() as u16).to_le_bytes();
                                                    //存储字符串长度
                                                    charData[0] = bytes[0];
                                                    charData[1] = bytes[1];
                                                    buf[index..(index + v.len() + 2)]
                                                        .copy_from_slice(&*charData);
                                                    index += (v.len() + 2);
                                                    // println!("修改={:?}", String::from_utf8(charData));
                                                }
                                                _ => {
                                                    buf[index..(index + v.len())].copy_from_slice(&v.data);
                                                    index += &v.data.len();
                                                }
                                            }
                                        }
                                    }
                                }

                                println!("准备修改数据");
                                /**
                                用一个循环数字标记当前最新有效数据，如果左边大则本次写到右边，如果右边大则写到左边
                                [0,0] 左边最大,[0,1]，右边最大,[2,1]，左边最大,[2,3] 右边最大,[0,3] 左边最大(3定义为极限，0最大),[0,1]
                                 */
                                let mut start = pos;
                                match end1 << end2 {
                                    _ => {}
                                    0 => {
                                        //[0,0]
                                        buf.push(1u8);
                                        start += (self.len + 2) as u64;
                                    }
                                    1 => {
                                        //[0,1]
                                        buf.push(2u8);
                                    }
                                    4 => {
                                        //[2,1]
                                        buf.push(3u8);
                                        start += (self.len + 2) as u64;
                                    }
                                    12 => {
                                        //[2,3]
                                        buf.push(0u8);
                                    }
                                    3 => {
                                        //[0,3]
                                        buf.push(1u8);
                                        start += (self.len + 2) as u64;
                                    }
                                }

                                let _ = file.seek(SeekFrom::Start(start));
                                // println!("file.write={:?}", buf);
                                file.write(&*buf).expect("write 更新失败");
                            }
                        },
                        &mut file,
                    );
                    file.sync_all().expect("sync_all 修改失败");
                    println!("准备修改完成");
                }

                pub fn createRow(&self) -> Row {
                    let mut row = Row::new(self.path.clone().to_string());
                    for col in self.columns.iter() {
                        row.add(&col);
                    }
                    row
                }
            }

            pub struct Row {
                pub data: HashMap<String, Value>,
                tag: String,
            }

            impl Row {
                fn new(tag: String) -> Row {
                    Self {
                        data: HashMap::new(),
                        tag,
                    }
                }

                pub fn eq(&self, name: &str, v: &Value) -> bool {
                    return match self.data.get(name) {
                        None => v.is_null(),
                        Some(a) => {
                            if v.is_null() {
                                return false;
                            }
                            a.eq(&v)
                        }
                    };
                }
                pub fn lt(&self, name: &str, v: Value) -> bool {
                    return match self.data.get(name) {
                        None => false,
                        Some(a) => {
                            if v.is_null() {
                                return false;
                            }
                            a.lt(&v)
                        }
                    };
                }
                pub fn le(&self, name: &str, v: Value) -> bool {
                    return match self.data.get(name) {
                        None => v.is_null(),
                        Some(a) => {
                            if v.is_null() {
                                return false;
                            }
                            a.le(&v)
                        }
                    };
                }
                pub fn gt(&self, name: &str, v: Value) -> bool {
                    return match self.data.get(name) {
                        None => v.is_null(),
                        Some(a) => {
                            if v.is_null() {
                                return false;
                            }
                            a.gt(&v)
                        }
                    };
                }
                pub fn ge(&self, name: &str, v: Value) -> bool {
                    return match self.data.get(name) {
                        None => v.is_null(),
                        Some(a) => {
                            if v.is_null() {
                                return false;
                            }
                            a.ge(&v)
                        }
                    };
                }

                pub fn add(&mut self, col: &Column) {
                    self.data.insert(col.name.to_string(), Value::new());
                }

                pub fn insert(&mut self, name: &str, value: Value) -> &Row {
                    match self.data.get(name) {
                        None => {
                            panic!("未定义字段:{}", name);
                        }
                        Some(a) => {
                            self.data.insert(name.to_string(), value);
                        }
                    }
                    self
                }
                pub fn set(&mut self, name: &str, value: Value) -> &Row {
                    match self.data.get(name) {
                        None => {
                            panic!("未定义字段:{}", name);
                        }
                        Some(a) => {
                            self.data.insert(name.to_string(), value);
                        }
                    }
                    self
                }
                pub fn get(&self, name: &str) -> Option<&Value> {
                    self.data.get(name)
                }
                pub fn getv(&self, name: &str) -> &Value {
                    &self.data.get(name).unwrap()
                }
            }

            pub struct Column {
                name: &'static str,
                columnType: Type,
            }

            impl Column {
                pub fn new(name: &'static str, columnType: Type) -> Self {
                    Self { name, columnType }
                }
                fn len(&self) -> usize {
                    match self.columnType {
                        Type::I8 => 1usize,
                        Type::I16 => 2usize,
                        Type::I32 => 4usize,
                        Type::I64 => 8usize,
                        Type::U8 => 1usize,
                        Type::U16 => 2usize,
                        Type::U32 => 4usize,
                        Type::U64 => 8usize,
                        Type::Float => 4usize,
                        Type::Double => 8usize,
                        Type::Char(s) => (s * 4) as usize,
                    }
                }
            }
        }
    ).into()
}
