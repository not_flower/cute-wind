use std::collections::HashSet;
use std::ops::{Add, Deref, DerefMut};

use fltk::{app, menu};
use fltk::app::{event, MouseButton, WidgetId};
use fltk::button::{Button, CheckButton};
use fltk::draw::{draw_rect_fill, draw_text2, set_draw_color};
use fltk::enums::{Align, Color, Event, FrameType};
use fltk::frame::Frame;
use fltk::group::Group;
use fltk::input::Input;
use fltk::menu::MenuButton;
use fltk::misc::InputChoice;
use fltk::prelude::{ButtonExt, GroupExt, ImageExt, InputExt, MenuExt, ValuatorExt, WidgetBase, WidgetExt};
use fltk::tree::{Tree, TreeConnectorStyle, TreeReason, TreeSelect};
use rand::Rng;
// use crate::RedisConnectInfo;
use redis::{Commands, Iter, RedisResult};

use crate::{connect_window, flush_left_group, MIUI, MSG_TYPE, RedisConnectInfo, RedisConnectInfo_db, w_param};
use crate::MIUI::{BTN_CONFIRM, center, MButton, MCheckButton, MDialog, MGroupInput, MInput, MInputChoice};
use crate::st::Value;
use crate::svg_common::SVG;

pub fn slide_prompt_red(txt: String){
    let mut bind = w_param.lock().unwrap();
    let (mut x, mut y, w, h) = *bind;
    // println!("x={},w={}",x,w);
    x = x + w / 2 - 100;
    MDialog::slide_prompt_red(txt.as_str(), x, y, 200, h);
}
pub fn slide_prompt(txt: String){
    let mut bind = w_param.lock().unwrap();
    let (mut x, mut y, w, h) = *bind;
    // println!("x={},w={}",x,w);
    x = x + w / 2 - 100;
    MDialog::slide_prompt(txt.as_str(), x, y, 200, h);
}


fn InputChoice(x:i32,y:i32,w:i32,h:i32)->InputChoice{
    let mut menu = InputChoice::miui(x,y,w,h, "");
    menu.set_frame(FrameType::FlatBox);

    menu.set_value("0");
    for db in 0..16 {
        menu.add(db.to_string().as_str());
    }
    menu
}

pub struct RedisTreeGroup {
    t_widget: fltk::group::Group,
}


impl RedisTreeGroup {
    pub fn new(x: i32, y: i32, w: i32, h: i32) -> Self {

        let mut group = Group::new(x, y, w, h, "");
        // group.set_frame(FrameType::FlatBox);
        // group.set_color(Color::Red);
        group.show();
        Self { t_widget: group }
    }
    pub fn group_addItem(t_widget: &mut Group, connect_info: &mut RedisConnectInfo)-> RedisTreeItemGroup {
        let g = t_widget;
        let (x, y, w, h) = (g.x(), g.y(), g.w(), g.h());
        let item_num = g.children();
        let mut item = RedisTreeItemGroup::new(x, y + 50 * item_num, w, h, item_num, connect_info.clone());
        g.add(&*item);
        item
    }
    pub fn group_clearItem(t_widget: &mut Group) {
        t_widget.clear();
        let keys = app::widget_ids("_TreeItem");
        for k in keys.iter() {
            app::remove_widget_id(k);
        }
        let keys = app::widget_ids("_content");
        for k in keys.iter() {
            app::remove_widget_id(k);
        }
    }
    pub fn addItemGroup(&mut self, connect_info: &mut RedisConnectInfo) -> RedisTreeItemGroup {
        let g = &self.t_widget;
        let (x, y, w, h) = (g.x(), g.y(), g.w(), g.h());
        let item_num = g.children();
        let item = RedisTreeItemGroup::new(x, y + 50 * item_num, w, h, item_num, connect_info.clone());
        self.t_widget.add(&*item);
        item
    }

    pub fn clear(&mut self) {
        self.t_widget.clear();
    }

    pub fn set_id(&mut self, id: &str) {
        self.t_widget.set_id(id);
    }
}

impl Deref for RedisTreeGroup {
    type Target = fltk::group::Group;
    fn deref(&self) -> &Self::Target {
        &self.t_widget
    }
}

impl DerefMut for RedisTreeGroup {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.t_widget
    }
}

pub struct RedisTreeItemGroup {
    t_widget_item: fltk::group::Group,
    t_widget_item_content: fltk::group::Group,
    connect_info: RedisConnectInfo,
    tree: Tree,
    id: String,
}

impl Deref for RedisTreeItemGroup {
    type Target = fltk::group::Group;
    fn deref(&self) -> &Self::Target {
        &self.t_widget_item
    }
}

impl DerefMut for RedisTreeItemGroup {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.t_widget_item
    }
}


impl RedisTreeItemGroup {
    pub fn new(x: i32, y: i32, w: i32, h: i32, item_num: i32, connect_info: RedisConnectInfo) -> Self {
        let mut t_widget_item = Group::new(x, y, w, h, "");
        let gid = item_num.to_string().add("_TreeItem");
        t_widget_item.set_id(gid.clone().as_str());
        t_widget_item.set_color(Color::White);
        let mut connect_name = connect_info.connect_name.clone();
        let mut f = Frame::new(x, y, w, 40, "");
        let mut rng = rand::thread_rng();
        let id = rng.gen_range(1..=1000000).to_string();
        f.set_id(&id);
        f.set_color(Color::White);
        f.set_label_size(14);
        let mut close_state = SVG::right_dir(None);
        close_state.scale(14,14,false,false);
        let mut open_state = SVG::down_dir(None);
        open_state.scale(14,14,false,false);

        f.draw(move |f| {
            if event() == Event::Push {
                draw_rect_fill(f.x(), f.y(), f.w(), f.h(), Color::rgb_color(231,242,255));
            } else {
                draw_rect_fill(f.x(), f.y(), f.w(), f.h(), Color::White);
            }
            set_draw_color(Color::Black);
            draw_text2(connect_name.as_str(), f.x()+10, f.y(), f.w(), f.h(), Align::Left);
            if f.label().len()==0 {
                close_state.draw(f.x()+f.w()-20, f.y()+(f.h()-14)/2, 14, 14);
            }else{
                open_state.draw(f.x()+f.w()-20, f.y()+(f.h()-14)/2, 14, 14);
            }
        });

        let mut menu = MenuButton::default()
            .size_of(&f)
            .center_of(&f)
            .with_type(menu::MenuButtonType::Popup3);
        let choice = "编辑连接|删除连接|清除所有键";
        menu.add_choice(choice);
        menu.set_frame(FrameType::BorderBox);
        menu.set_color(Color::White);
        let mut connect_info_cp = connect_info.clone();
        menu.set_callback(move |m|{
            if let Some(t) = m.choice() {
                if t.as_str() == "清除所有键" {

                    let (x,y) = MIUI::center(300,60);
                    let mut dialog = MDialog::new(x, y, 300, 60, "清空数据库");

                    //Frame::new(5,5,200,30,"选择库").with_align(Align::Left|Align::Inside);
                    let mut choice = InputChoice(60,15,200,30);
                    choice.set_label("选择库:");
                    choice.set_value("");
                    dialog.add_content(&choice);

                    let mut cancel = Button::default().with_size(50, 20).with_label("取消");
                    dialog.add_btn(&mut cancel);
                    cancel.set_callback(|b| b.window().unwrap().hide());

                    let mut confirm = Button::default().with_size(50, 20).with_label("确定");
                    let mut connect_info_cp = connect_info_cp.clone();
                    confirm.set_callback(move |b| {
                        if let Some(t) = choice.value() {
                            if t.len() == 0 {
                                choice.set_label_color(Color::Red);
                                let mut choice = choice.clone();
                                choice.parent().unwrap().redraw();
                                app::add_timeout3(0.3,move |h|{
                                    choice.set_label_color(Color::Black);
                                    choice.parent().unwrap().redraw();
                                });

                                return ;
                            }else {
                                let db= t.parse::<i8>().unwrap();
                                connect_info_cp.set_db(db);
                                let conn_res = connect_info_cp.connect();
                                if let Ok(mut conn) = conn_res {
                                    let del_res:RedisResult<i64> = conn.del("*");
                                    if let Ok(n) = del_res {
                                        b.window().unwrap().hide();
                                        slide_prompt(format!("库{} 清除完毕", db));
                                    }
                                }

                            }
                        }
                        
                    });
                    dialog.add_btn(&mut confirm);

                    dialog.show();

                }
                if t.as_str() == "删除连接" {
                    let mut connect_info_cp = connect_info_cp.clone();
                    MDialog::confirm("确定删除连接?",move|b|{
                        let mut db = RedisConnectInfo_db::new();
                        db.delete(|r|r.eq("id", &Value::from_str(connect_info_cp.id.clone().as_str())));
                        b.window().unwrap().hide();
                        flush_left_group();
                        slide_prompt("删除完毕".to_string());
                    }).show();
                }
                if t.as_str() == "编辑连接" {
                    connect_window(Some(connect_info_cp.clone()));
                }
            }
            m.clear();
            m.add_choice(choice);
        });


        let item_content_id = id.clone().add("_content");
        let mut t_widget_item_content = Group::new(f.x(), f.y() + 40, w, h, "");
        t_widget_item_content.set_id(&id.clone().add("_content"));
        t_widget_item_content.set_frame(FrameType::FlatBox);
        t_widget_item_content.set_color(Color::White);
        let mut tree = Tree::new(f.x(), f.y() + 100, 200, 445, "");
        tree.set_scrollbar_size(10);
        tree.set_frame(FrameType::FlatBox);

        // tree.draw(|d|{
        //     set_draw_color(global_border_color);
        //     draw_line(d.x()+10,d.y()+d.h(),d.x()+d.w()-20,d.y()+d.h());
        // });

        let mut tree_cp = tree.clone();
        let mut connect_info_cp = connect_info.clone();

        f.handle(move |f, e| {

            let mouse_button = app::event_mouse_button();
            if mouse_button == MouseButton::Right{
                return true;
            }

            if e == Event::Push {
                f.redraw();
                return true;
            }
            if e == Event::Released {

                f.redraw();
                let mut g: Group = app::widget_from_id(&item_content_id).unwrap();
                //按照_TreeItem顺序重新设置位置
                let keys = app::widget_ids("_TreeItem");
                for k in keys.iter() {
                    let mut other_group: Group = app::widget_from_id(k.as_str()).unwrap();
                    let mut split_iter = k.split("_");
                    if let Some(first_word) = split_iter.next() {
                        let num: i32 = first_word.parse().unwrap();
                        other_group.set_pos(other_group.x(), 50 + 50 * num);
                    }
                }

                if g.visible() {
                    let keys = app::widget_ids("_content");
                    for k in keys.iter() {
                        let mut other_group: Group = app::widget_from_id(k.as_str()).unwrap();
                        other_group.parent().unwrap().show();
                    }
                    g.hide();
                    f.set_label("");
                } else {
                    //给展开的tree腾出位置
                    let keys = app::widget_ids("_content");
                    for k in keys.iter() {
                        let mut other_group: Group = app::widget_from_id(k.as_str()).unwrap();
                        other_group.parent().unwrap().hide();

                    }
                    g.show();
                    let mut g = g.parent().unwrap();
                    let mut p = g.parent().unwrap();
                    let i = p.find(&g);
                    p.remove_by_index(i);
                    p.insert(&g,0);
                    g.set_pos(p.x(),p.y());
                    RedisTreeItemGroup::loadTreeData(&mut tree_cp, &connect_info_cp, 0, false, "");
                    g.show();

                    f.set_label(" ");
                }
                return true;
            }
            false
        });



        let mut choice_db = InputChoice(f.x()+2, f.y() + 40, 80, 26);
        let mut connect_info_cp = connect_info.clone();
        let mut tree_cp = tree.clone();
        //切换数据库
        choice_db.set_callback(move |c|{
            let db_str= c.value().unwrap();
            if db_str.len()>0 {
                let db = db_str.parse::<i8>().unwrap();
                {
                    let mut bind = connect_info_cp.db.lock().unwrap();
                    *bind = db;
                }
                RedisTreeItemGroup::loadTreeData(&mut tree_cp, &connect_info_cp, 0, false, "");
            }
        });

        let mut new_add_key = Button::miui(90, f.y() + 40, 50, 26, "新增");

        new_add_key.btn_color(Color::from_rgb(235, 237, 234));
        new_add_key.set_label_color(Color::Black);
        let mut tree_cp = tree.clone();
        let mut connect_info_cp = connect_info.clone();
        new_add_key.set_callback(move |a| {
            let (x,y) = center(350, 180);
            let mut md = MDialog::new(x, y, 350, 180, "新增Key");
            md.begin();

            Frame::new(0, 0, 100, 30, "key名");
            let key_name = Input::miui(20, 30, 300, 30, "");

            Frame::new(0, 60, 100, 30, "key类型");
            let mut key_type = InputChoice::miui(20, 90, 300, 30, "");
            key_type.set_value("string");

            let key_types = MSG_TYPE::get_key_types();
            for t in key_types {
                key_type.add(t.as_str());
            }
            md.end();
            let mut cancel = Button::miui(270, 140, 60, 30, "取消");
            cancel.set_callback(|b| b.window().unwrap().hide());
            md.add_btn(&mut cancel);

            let mut confirm = Button::miui(200, 140, 60, 30, "确定");
            md.add_btn(&mut confirm);
            confirm.btn_color(BTN_CONFIRM);
            let mut connect_info_cp = connect_info_cp.clone();
            let mut tree_cp = tree_cp.clone();
            confirm.set_callback(move |b| {
                let key_name = key_name.value();
                let key_type = key_type.value().unwrap();
                let msg_type = MSG_TYPE::get(key_type.as_str());
                if key_name.trim().len()==0 {
                    b.window().unwrap().hide();
                    return ;
                }
                let mut res = connect_info_cp.connect();

                match res {
                    Ok(mut con) => {
                        println!("1");
                        let mut call = |key_name: &str| {
                            let root_op = tree_cp.root();
                            match root_op {
                                None => {
                                    tree_cp.add(key_name);
                                }
                                Some(root) => {
                                    tree_cp.insert(&root, key_name, 0);
                                }
                            }

                            tree_cp.hide();
                            tree_cp.show();
                        };

                        match msg_type {
                            MSG_TYPE::STRING => {
                                let res = con.set::<&str, _, String>(&key_name, "".to_string());
                                ;
                                match res {
                                    Ok(_) => call(&key_name),
                                    Err(e) => slide_prompt_red(format!("创建失败:{}", e))
                                }
                            }
                            MSG_TYPE::HASH => {
                                let res = con.hset::<&str, _, String, i64>(&key_name, "new field".to_string(), "new value".to_string());
                                match res {
                                    Ok(_) => call(&key_name),
                                    Err(e) => slide_prompt_red(format!("创建失败:{}", e))
                                }
                            }
                            MSG_TYPE::LIST => {
                                let res = con.rpush::<&str, _, i64>(&key_name, "new value".to_string());
                                match res {
                                    Ok(_) => call(&key_name),
                                    Err(e) => slide_prompt_red(format!("创建失败:{}", e))
                                }
                            }
                            MSG_TYPE::SET => {
                                let res = con.sadd::<&str, _, i64>(&key_name, "new value".to_string());
                                match res {
                                    Ok(_) => call(&key_name),
                                    Err(e) => slide_prompt_red(format!("创建失败:{}", e))
                                }
                            }
                            MSG_TYPE::ZSET => {
                                let res = con.zadd::<&str, _, String, i64>(&key_name, "new value".to_string(), "0".to_string());
                                match res {
                                    Ok(_) => call(&key_name),
                                    Err(e) => slide_prompt_red( format!("创建失败:{}", e))
                                }
                            }
                            MSG_TYPE::STREAM => {
                                let res = con.xadd::<_, _, &str, &str, String>(&key_name.as_str(), "*", &[("new field", "new value"), ("new field2", "new value2")]);
                                match res {
                                    Ok(_) => call(&key_name),
                                    Err(e) => slide_prompt_red(format!("创建失败:{}", e))
                                }
                            }
                            _ => {}
                        }
                        b.window().unwrap().hide();
                    }
                    Err(e) => {
                        println!("-1");
                        slide_prompt_red(format!("获取连接失败:{}", e))
                    }
                }
            });


            md.show();
        });

        let mut delete_key = Button::miui(145, f.y() + 40, 50, 26, "删除");
        delete_key.btn_color(Color::from_rgb(235, 237, 234));
        delete_key.set_label_color(Color::Black);
        let mut tree_cp = tree.clone();
        let mut connect_info_cp = connect_info.clone();
        delete_key.set_callback(move |b|{
            let selected_op = tree_cp.get_selected_items();
            match selected_op {
                None => {
                    slide_prompt("请选择需要删除的keys".to_string());
                }
                Some(vec) => {

                    let mut tree_cp = tree_cp.clone();
                    let mut connect_info_cp = connect_info_cp.clone();
                    MDialog::confirm("确认要删除所选key?",move |b|{

                        let connect_res = connect_info_cp.connect();
                        if let Ok(mut connect) = connect_res {
                            for t in &vec {
                                let k = t.label().unwrap();
                                let del_res:RedisResult<i32> = connect.del(k);
                                if let Err(e) = del_res {
                                    RedisTreeItemGroup::loadTreeData(&mut tree_cp, &connect_info_cp, 0, false, "");
                                    slide_prompt_red(format!("错误:{}",e));
                                    return;
                                }
                            }
                            RedisTreeItemGroup::loadTreeData(&mut tree_cp, &connect_info_cp, 0, false, "");
                            slide_prompt("删除完毕".to_string());
                        }
                        b.window().unwrap().hide();
                    }).show();

                }
            }
        });

        let mut query_input = MGroupInput::new(f.x(), f.y() + 70, 200, 30);
        let mut query_btn = Button::default().with_size(20, 20);
        query_btn.set_image(Some(SVG::magnifier(Some(Color::rgb_color(110,110,110)))));
        query_input.add(&query_btn);
        query_btn.set_frame(FrameType::NoBox);
        query_btn.set_color(Color::White);
        query_btn.set_tooltip("搜索");


        let mut choice = <CheckButton as MCheckButton>::miui_default();
        choice.set_size(12,12);
        choice.set_color(Color::rgb_color(180,180,180));
        choice.set_tooltip("开启精准匹配");
        query_input.add(&choice.to_owned());

        let mut tree_cp = tree.clone();
        let mut connect_info_cp = connect_info.clone();
        query_btn.set_callback(move |b|{
            let word = query_input.value();
            let word= word.as_str();
            RedisTreeItemGroup::loadTreeData(&mut tree_cp, &connect_info_cp, 0, choice.value(), word);
        });

        tree.set_select_mode(TreeSelect::Multi);

        tree.set_show_root(false);
        tree.set_select_frame(FrameType::FlatBox);
        tree.set_selection_color(Color::rgb_color(235,235,235));
        tree.visible_focus(false);
        tree.set_show_collapse(false);
        tree.set_connector_style(TreeConnectorStyle::None);
        tree.set_trigger(fltk::enums::CallbackTrigger::ReleaseAlways);
        tree.hide();
        tree.set_connector_width(0);
        tree.set_label_margin_left(5);
        tree.set_widget_margin_left(0);
        tree.set_item_label_size(14);
        tree.set_margin_left(0);
        // tree.is_hscroll_visible()
        // tree.set_scrollbar_size(10);

        // tree.draw(|t| {
        //     if event() == Event::Push {
        //         //let items = t.get_selected_items();
        //         if let Some(items) = t.get_selected_items() {
        //             if items.len() > 0 {
        //                 let item = items.get(0).unwrap();
        //                 //println!("item={}",item.label().unwrap());
        //                 let (x, y, w, h) = (item.x(), item.y(), item.w(), item.h());
        //                 draw_line(x, y + h, x + w, y + h);
        //                 draw_line(x, y, x + w, y);
        //             }
        //         }
        //     }
        // });

        tree.end();
        t_widget_item_content.hide();
        t_widget_item_content.end();
        t_widget_item.end();
        t_widget_item.show();


        return Self { t_widget_item, t_widget_item_content, connect_info, tree, id: gid.clone() };
    }
    //tree节点的选择事件
    pub fn selected<F: Fn(&mut Tree,&mut RedisConnectInfo, &str, &str) + 'static>(&mut self, f: F) {
        let mut cn = self.connect_info.clone();
        let id = self.id();

        self.tree.set_callback(move |mut t| {
            if event() == Event::Push && t.callback_reason()==TreeReason::Selected{
                if let Some(item) = t.callback_item() {
                    f(&mut t,&mut cn, id.as_str(), item.label().unwrap().as_str());
                }
            }

        });
    }

    pub fn get_tree(&self) -> &Tree {
        &self.tree
    }

    //precise=true开启精准匹配，word=“待匹配的关键字”
    pub fn loadTreeData(tree: &mut Tree, connect_info: &RedisConnectInfo, db: i64,precise:bool,ketword:&str) {
        tree.clear();
        tree.hide();
        let mut connection = connect_info.connect();

        match connection {
            Ok(mut con) => {
                //如果没有关键字，则匹配所有
                if ketword.len()==0 {
                    // for i in 0..10000 {
                    //     let mut k=i.to_string();
                    //     k.push_str("_test");
                    //     con.set::<&str, _, String>(k.as_str(), i.to_string());
                    // }
                    let mut set:HashSet<String> = HashSet::new();
                    let ks:Iter<String> = con.scan().unwrap();
                    //只加载1000条数据
                    for k in ks {
                        if !set.contains(k.as_str()) {
                            set.insert(k.to_string());
                            tree.add(k.as_str());
                            if set.len() > 40 {
                                break
                            }
                            continue;
                        }


                    }
                    // let ks: Vec<String> = con.keys("*").unwrap();
                    // for k in ks {
                    //     tree.add(k.as_str());
                    // }
                }else{
                    if precise {
                        let exists_res:RedisResult<i32> = con.exists(ketword);
                        if let Ok(n) = exists_res {
                            if n >0 {
                                tree.add(ketword);
                            }
                        }
                    }else{
                        let mut pattern = String::from("*");
                        pattern.push_str(ketword);
                        pattern.push_str("*");
                        let ks: Vec<String> = con.keys(pattern.as_str()).unwrap();
                        //println!("{:?}",ks);
                        let mut n = 0;
                        for k in ks {
                            n+=1;
                            tree.add(k.as_str());
                            if n>1000 {
                                break
                            }
                        }
                    }
                }

            }
            Err(e) => {
                slide_prompt_red(format!("连接失败:{}", e))
            }
        }

        tree.show();
    }
    pub fn id(&self) -> String {
        self.id.clone()
    }
}