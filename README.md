# cute-wind

#### 介绍
这是一个采用用rust+fltk编写的redis管理工具。
它具有体积很小、速度快、内存占用低等优点。
![程序包800多k](src/assets/1695112321094.jpg)
![内存占用5m](src/assets/image.png)

这个工具打包后的程序只有几百k，这个是我中意使用fltl框架的原因。说实话，这个框架很老，好多好多效果做不出来。比HTML4差老鼻子远了，更不用说炫到不得了的H5了。
但是它小啊，然后还能跨平台。额...跨平台的事，等工能齐备了再适配吧，先管windows。 :smile: 

目前这个只能创建单个节点，基础的查询编辑等功能已经完成。
希望能在春节前出一个完整的测试版，年后出一个正式版。


#### 安装教程

1.  绿色免安装的
2.  首次打开的时候，会初始化一个数据文件。最好把程序单独放到一个文件夹里面


#### 使用说明

1.  ![输入图片说明](https://foruda.gitee.com/images/1695111160189374418/b5972fdf_8202700.png "屏幕截图") 当前只能创建单个节点
2.  ![输入图片说明](https://foruda.gitee.com/images/1695111224461214452/26792e7e_8202700.png "屏幕截图")




